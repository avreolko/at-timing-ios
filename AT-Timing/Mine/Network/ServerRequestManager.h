//
//  ServerRequestManager.h
//  DEM
//
//  Created by Valentin Cherepyanko on 03/12/14.
//
//

#define TIMEOUT 60

#import <Foundation/Foundation.h>
#import "ServerSettings.h"
#import "NiceResponse.h"

@interface ServerRequestManager : NSObject <NSURLSessionDelegate>
- (void)GETRequestWithURL:(NSURL *)url completion:(void (^)(NiceResponse *response))callback;
- (void)POSTRequestWithURL:(NSURL *)url andData:(NSString *)data completion:(void (^)(NiceResponse *response))callback;
- (void)DELETERequestWithURL:(NSURL *)url completion:(void (^)(NiceResponse *response))callback;
- (void)PUTRequestWithURL:(NSURL *)url andData:(NSString *)data completion:(void (^)(NiceResponse *response))callback;


@end
