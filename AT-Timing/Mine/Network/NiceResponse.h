//
//  NiceResponse.h
//  Transport
//
//  Created by Valentin Cherepyanko on 20/10/15.
//  Copyright © 2015 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NiceResponse : NSObject

@property NSURLResponse *urlResponse;
@property NSString *dataString;
@property NSInteger statusCode;

@end
