//
//  ServerSettings.h
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//


#define TimesheetID @"317d30ea-8fcd-7f78-82ee-208cc219f16d"
#define ProductID @"c01f336a-100d-8ad7-480f-1564173765bb"
#define SubjectID @"9cd7150a-0f30-2a00-3051-07d2308b8ce5"
#define ProjectID @"3072c293-dc52-79e4-ca9c-4f1b37a2d880"
#define StatusTimeID @"e4ece46e-ff56-127a-ced6-b4f3054b44a4"

#define SERVER_ADDRESS @"erp.at-sibir.ru/rest"
//#define SERVER_ADDRESS @"erp.point.at-sibir.ru/rest"

#define HTTP_PORT 0

#define PING_URL @"ping"
#define AUTH_URL @"login"
#define CHECK_LOGIN_URL @"data/entity?entityId=0d4363dd-41fe-1248-b76d-e9b3bafadddf&attributes=login,fio,jobTitle.name&search="
#define NEW_CHECK_LOGIN_URL @"data/entity?$employeeLogin="
#define PROJECT_SEARCH_URL @"data/entity?entityId=3072c293-dc52-79e4-ca9c-4f1b37a2d880&attributes=code&search="
#define ENTITY_URL @"data/entity"
#define ADD_WORKLOG_URL @"data/entity/30cf2ce4-4d4c-50f7-6f8c-04b308ea95d3"
#define DELETE_ENTITY_URL @"data/deleteentity"
#define ALL_TICKETS_URL @"jira/lastTickets"
#define JIRA_LOG_URL @"jira/timeSpent"
#define JIRA_DELETE_LOG_URL @"jira/deleteWorklog"
#define TICKET_SEARCH_URL @"jira/searchTickets"

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ServerSettings : NSObject

+ (NSURL *)pingURL;
+ (NSURL *)authURL;
+ (NSURL *)checkLoginUrl:(NSString *)login;
+ (NSURL *)newCheckLoginUrl:(NSString *)login;
+ (NSURL *)entityURL;
+ (NSURL *)projectSearchURL:(NSString *)search;
+ (NSURL *)addWorklogURL;
+ (NSURL *)deleteEntityURL;
+ (NSURL *)editEntityURL:(NSString *)objectID;
+ (NSURL *)entitySearchURL:(NSString *)entityID search:(NSString *)search;
+ (NSURL *)addObjectURL:(NSString *)objectID;
+ (NSURL *)allTicketsURL;
+ (NSURL *)jiraLogURL:(NSString *)ticket;
+ (NSURL *)jiraDeleteLogURL:(NSString *)tickedCode logID:(NSString *)logID;
+ (NSURL *)jiraEditLogURL:(NSString *)tickedCode logID:(NSString *)logID;
+ (NSURL *)ticketSearchURL:(NSString *)search;

+ (NSURL *)timesheetURL:(NSString *)userID;
+ (NSURL *)addAttributeToURL:(NSURL *)url attr:(NSString *)attr value:(NSString *)value;

@end
