//
//  ServerSettings.m
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//

#import "ServerSettings.h"
#import "AuthManager.h"

#define PORT 34234234
@implementation ServerSettings

+ (NSString *)getServerAddressString {
    NSString *serverAddress = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"SERVER_ADDRESS"];
    
    if (PORT > 0 && PORT < 65535) {
        return [NSString stringWithFormat:@"http://%@:%d", serverAddress, PORT];
    } else {
        return [NSString stringWithFormat:@"http://%@", serverAddress];
    }
}

+ (NSURL *)pingURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], PING_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)authURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], AUTH_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)checkLoginUrl:(NSString *)login {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", [self getServerAddressString], CHECK_LOGIN_URL, login];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)newCheckLoginUrl:(NSString *)login {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", [self getServerAddressString], NEW_CHECK_LOGIN_URL, login];
	return [NSURL URLWithString:urlString];
}

+ (NSURL *)entityURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], ENTITY_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)timesheetURL:(NSString *)userID {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@?$employee=%@", [self getServerAddressString], ENTITY_URL, userID];
	return [NSURL URLWithString:urlString];
}

+ (NSURL *)addWorklogURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], ADD_WORKLOG_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)projectSearchURL:(NSString *)search {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", [self getServerAddressString], PROJECT_SEARCH_URL, search];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)deleteEntityURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], DELETE_ENTITY_URL];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)editEntityURL:(NSString *)objectID {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], ENTITY_URL, objectID];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)entitySearchURL:(NSString *)entityID search:(NSString *)search {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@?entityId=%@&search=%@&entry=10", [self getServerAddressString], ENTITY_URL, entityID, search];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)addObjectURL:(NSString *)objectID {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], ENTITY_URL, objectID];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)allTicketsURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], ALL_TICKETS_URL, [AuthManager get].profile.login];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)jiraLogURL:(NSString *)ticket {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@", [self getServerAddressString], JIRA_LOG_URL, ticket, [AuthManager get].profile.login];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}
//DELETE deleteWorklog/{issueKey}/{worklogId}
+ (NSURL *)jiraDeleteLogURL:(NSString *)tickedCode logID:(NSString *)logID {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@", [self getServerAddressString], JIRA_DELETE_LOG_URL, tickedCode, logID];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)jiraEditLogURL:(NSString *)tickedCode logID:(NSString *)logID {
    NSString *login = [AuthManager get].profile.login;
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@", [self getServerAddressString], JIRA_LOG_URL, tickedCode, login, logID];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)ticketSearchURL:(NSString *)search {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], TICKET_SEARCH_URL, search];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)addAttributeToURL:(NSURL *)url attr:(NSString *)attr value:(NSString *)value {
    NSString *stringURL = url.absoluteString;
    NSString *add = [NSString stringWithFormat:@"&%@=%@", attr, value];
    stringURL = [stringURL stringByAppendingString:add];
    
    return [NSURL URLWithString:stringURL];
}

@end
