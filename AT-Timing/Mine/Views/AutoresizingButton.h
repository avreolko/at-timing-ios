//
//  AutoresizingButton.h
//  Conf
//
//  Created by Valentin Cherepyanko on 31/07/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProcessor.h"

@interface AutoresizingButton : UIButton

- (void)resize;
- (void)changeImage:(UIImage *)image;

@end
