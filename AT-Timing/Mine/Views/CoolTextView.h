//
//  CoolTextView.h
//  SwiftsMobile
//
//  Created by Valentin Cherepyanko on 27/01/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoolTextView : UITextView

/**
 The string that is displayed when there is no other text in the text view. This property reads and writes the
 attributed variant.
 The default value is `nil`.
 */
@property (nonatomic, strong) IBInspectable NSString *placeholder;

/**
 The attributed string that is displayed when there is no other text in the text view.
 The default value is `nil`.
 */
@property (nonatomic, strong) NSAttributedString *attributedPlaceholder;

/**
 Returns the drawing rectangle for the text views’s placeholder text.
 @param bounds The bounding rectangle of the receiver.
 @return The computed drawing rectangle for the placeholder text.
 */
- (CGRect)placeholderRectForBounds:(CGRect)bounds;

- (void)setRed:(BOOL)red;

@end
