//
//  AdjustableBlurEffect.m
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 08/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import "AdjustableBlurEffect.h"
#import <objc/runtime.h>


@interface UIBlurEffect (Protected)
@property (nonatomic) id effectSettings;
@end

@implementation AdjustableBlurEffect

+ (instancetype)effectWithStyle:(UIBlurEffectStyle)style
{
    id result = [super effectWithStyle:style];
    object_setClass(result, self);
    
    return result;
}

- (id)effectSettings
{
    id settings = [super effectSettings];
    [settings setValue:@10 forKey:@"blurRadius"];
    return settings;
}

- (id)copyWithZone:(NSZone*)zone
{
    id result = [super copyWithZone:zone];
    object_setClass(result, [self class]);
    return result;
}

@end