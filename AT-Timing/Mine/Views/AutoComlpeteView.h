//
//  AutoComlpeteView.h
//  DEM
//
//  Created by Valentin Cherepyanko on 30/09/15.
//
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Animations.h"
#import "ShapedButton.h"
#import "AutocompleteEntry.h"
#import "KeyboardEventsNotifier.h"

#define MAX_HEIGHT 188
#define PADDING 5

@protocol AutoCompleteViewDelegate

@optional
- (void)didSelectVariant:(NSUInteger)index;
@end

@interface AutoComlpeteView : UIView <KeyboardEventListener>

@property (weak) NSObject<AutoCompleteViewDelegate> *delegate;
@property UIScrollView *scrollView;
@property (weak) UIView *anchorView;

//+ (AutoComlpeteView *)initWith:(NSArray *)variants andCoords:(CGPoint)point andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate;
+ (AutoComlpeteView *)initWith:(NSArray *)variants andTextField:(UIView *)anchorView andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate;
+ (AutoComlpeteView *)initWith:(NSArray *)variants andCoords:(CGPoint)point andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate w:(CGFloat)width h:(CGFloat)height;

@end
