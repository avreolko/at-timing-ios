//
//  AutoComlpeteView.m
//  DEM
//
//  Created by Valentin Cherepyanko on 30/09/15.
//
//

#import "AutoComlpeteView.h"
#import "Utils.h"
#import "Masonry.h"

@implementation AutoComlpeteView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self hide];
    [self showAnimated];
}

+ (AutoComlpeteView *)initWith:(NSArray *)variants andCoords:(CGPoint)point andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate w:(CGFloat)width h:(CGFloat)height  {
    NSUInteger count = [variants count];
    
    AutoComlpeteView *view = [[self alloc] initWithFrame:CGRectMake(point.x, point.y, width, height)];
    
    view.delegate = delegate;
    
    view.backgroundColor = [Utils getColorFromInt:MAIN_COLOR];
    view.layer.cornerRadius = 3.0f;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.2;
    view.layer.shadowRadius = 3;
    view.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    
    view.scrollView = [[UIScrollView alloc] initWithFrame:view.bounds];
    [view addSubview:view.scrollView];
    
    [view createScrollViewConstraints];
    
    int i = 0;
    CGFloat offset = 0;
    for (NSString *variant in variants) {
        
        ShapedButton *button = [[ShapedButton alloc] initWithFrame:CGRectMake(5, 40 * i + 5, width - 10, 35)];
        button.cornerRadius = 3;
        button.tag = i;
        button.backgroundColor = [UIColor whiteColor];
        button.tintColor = [Utils getColorFromInt:SECOND_COLOR];
        [button setTitleColor:[Utils getColorFromInt:SECOND_COLOR] forState:UIControlStateHighlighted];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        
        button.titleLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
        button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [button setTitle:variant forState:UIControlStateNormal];
        [button addTarget:view action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [button layoutSubviews];
        float labelHeight = [Utils getHeightForLabel:button.titleLabel];
        button.frame = CGRectMake(5, 40 * i + 5 + offset, width - 10, labelHeight + 10);
        offset += button.titleLabel.frame.size.height - 25;
        
        [view.scrollView addSubview:button];
        i++;
    }
    [view.scrollView setContentSize:CGSizeMake(width, count * 40 + 5 + offset)];
    
    if (view.scrollView.contentSize.height > height) {
        view.frame = CGRectMake(point.x, point.y, width, height);
    } else {
        view.frame = CGRectMake(point.x, point.y, width, view.scrollView.contentSize.height);
    }
    
    return view;
}

+ (AutoComlpeteView *)initWith:(NSArray *)variants andTextField:(UIView *)anchorView andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate {
    CGFloat x = anchorView.frame.origin.x;
    CGFloat y = anchorView.frame.origin.y + anchorView.frame.size.height + PADDING;
    CGFloat viewHeight = [AutoComlpeteView calculateHeight:anchorView];
    
    if ([KeyboardEventsNotifier get].keyboardIsHere) {
        viewHeight -= 220;
    }
    
    AutoComlpeteView *ac = [AutoComlpeteView initWith:variants andCoords:CGPointMake(x, y) andDelegate:delegate w:anchorView.frame.size.width h:viewHeight];
    [[KeyboardEventsNotifier get] addListener:ac];
    ac.anchorView = anchorView;
    return ac;
}

+ (CGFloat)calculateHeight:(UIView *)anchorView {
    CGFloat x = anchorView.frame.origin.x;
    CGFloat y = anchorView.frame.origin.y + anchorView.frame.size.height + PADDING;
    
    CGPoint viewAnchorPoint = CGPointMake(x, y);
    CGPoint globalPoint = [[[UIApplication sharedApplication] keyWindow] convertPoint:viewAnchorPoint fromView:anchorView.superview];
    CGFloat screenHeight = Utils.screenSize.height;
    CGFloat viewHeight = screenHeight - globalPoint.y - PADDING * 2;
    if (viewHeight < 0) viewHeight = MAX_HEIGHT;
    return viewHeight;
}

- (void)createScrollViewConstraints {
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
        make.edges.equalTo(self).with.insets(insets);
    }];
}

- (void)buttonTapped:(UIButton*)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectVariant:)]) {
        [self.delegate didSelectVariant:sender.tag];
    }
    
    [self hideAnimatedWithCompletion:^{
        [self removeFromSuperview];
    }];
}

- (void)keyboardWillAppear {
    CGRect frame = self.frame;
    frame.size.height = [AutoComlpeteView calculateHeight:self.anchorView] - 220;
    self.frame = frame;
}

- (void)keyboardWillDisapper {
    CGRect frame = self.frame;
    CGFloat height = [AutoComlpeteView calculateHeight:self.anchorView];
    if (height < _scrollView.contentSize.height) {
        frame.size.height = height;
        self.frame = frame;
    }
}

@end
