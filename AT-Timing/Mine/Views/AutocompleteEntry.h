//
//  AutocompleteEntry.h
//  Transport
//
//  Created by Valentin Cherepyanko on 06/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AutocompleteEntry : NSObject

@property NSString *name;
@property CLLocation *location;

+ (AutocompleteEntry *)initWithPlaceDict:(NSDictionary *)dict;

@end
