//
//  CoolView.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CoolView : UIView

@property IBInspectable float alpha;
@property IBInspectable float cornerRadius;
@property IBInspectable UIColor *color;

@end
