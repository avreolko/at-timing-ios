//
//  ImageProcessor.m
//  ImageProcessor
//
//  Created by Valentin Cherepyanko on 24/11/14.
//  Copyright (c) 2014 Valentin Cherepyanko. All rights reserved.
//  

#import "ImageProcessor.h"


@implementation ImageProcessor

+ (NSData *)getDataFromImage:(UIImage *)image {
    @autoreleasepool
    {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.7);
        return imageData;
    }
}

+ (UIImage *)getImageFromData:(NSData *)data {
    return [UIImage imageWithData:data];
}

+ (UIImage *)rotateImage:(UIImage *)image {
    CGSize size = image.size;
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    [[UIImage imageWithCGImage:[image CGImage] scale:1.0 orientation:UIImageOrientationLeft] drawInRect:CGRectMake(0,0,size.height ,size.width)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)invertColors:(UIImage *)image {
    UIGraphicsBeginImageContext(image.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    [image drawInRect:imageRect];
    
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDifference);
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, image.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    CGContextClipToMask(UIGraphicsGetCurrentContext(), imageRect,  image.CGImage);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor whiteColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height));
    
    UIImage *returnImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return returnImage;
}

+ (UIImage *)mirrorImage:(UIImage *)image {
    UIGraphicsBeginImageContext(image.size);
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(currentContext, image.size.width, 0);
    CGContextScaleCTM(currentContext, -1.0, 1.0);
    
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    UIImage *mirroredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return mirroredImage;
}

+ (UIImage *)compressImage:(UIImage *)image {
    NSData *imgData= UIImageJPEGRepresentation(image, 0.7);
    UIImage *compressedImage = [UIImage imageWithData:imgData];
    return compressedImage;
}

+ (UIImage *)getResizedImage:(UIImage *)image forSize:(int)size {
    if (size > image.size.width && size > image.size.height) {
        return image;
    }
    
    int maxParam = (image.size.width > image.size.height) ? image.size.width : image.size.height;
    
    CGFloat multiplier = maxParam / size;
    
    CGSize newSize;
    newSize.width = image.size.width / multiplier;
    newSize.height = image.size.height / multiplier;
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageWithName:(NSString *)name andColor:(UIColor *)color {
    UIImage *image = [UIImage imageNamed:name];
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithName:(NSString *)name andColor:(UIColor *)color andSize:(int)size {
    UIImage *image = [self imageWithName:name andColor:color];
    return [self getResizedImage:image forSize:size];
}

+ (UIImage*)imageSaturated:(UIImage *)image value:(CGFloat)value {
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *ciimage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIColorControls"];
    [filter setValue:ciimage forKey:kCIInputImageKey];
    [filter setValue:@(value) forKey:kCIInputSaturationKey];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    CGImageRef cgImage = [context createCGImage:result fromRect:[result extent]];
    UIImage *simage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return simage;
}

+ (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
