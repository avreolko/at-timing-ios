//
//  TransitionHelper.m
//  Calca
//
//  Created by Valentin Cherepyanko on 02/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "TransitionHelper.h"

@implementation TransitionHelper

+ (CATransition *)toLeft {
    CATransition* transition = [CATransition animation];
    transition.duration = .4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    return transition;
}

+ (CATransition *)toRight {
    CATransition* transition = [CATransition animation];
    transition.duration = .4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    
    return transition;
}

+ (CATransition *)over {
    CATransition* transition = [CATransition animation];
    transition.duration = .4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    
    return transition;
}

@end
