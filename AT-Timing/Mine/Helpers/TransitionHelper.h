//
//  TransitionHelper.h
//  Calca
//
//  Created by Valentin Cherepyanko on 02/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TransitionHelper : NSObject

+ (CATransition *)toLeft;
+ (CATransition *)toRight;
+ (CATransition *)over;

@end
