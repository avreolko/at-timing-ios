//
//  Cache.m
//  Transport
//
//  Created by Valentin Cherepyanko on 19/11/15.
//  Copyright © 2015 Valentin Cherepyanko. All rights reserved.
//

#import "Cache.h"

@implementation Cache

+ (Cache *)get {
    static Cache *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.objects = [NSMutableDictionary new];
        [instance load];
    });
    return instance;
}

- (void)save {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults rm_setCustomObject:self.objects forKey:CACHE_KEY];
    NSLog(@"Cached object with key: %@", CACHE_KEY);
}

- (void)load {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *loadedObjects = [defaults rm_customObjectForKey:CACHE_KEY];
    
    if (loadedObjects) self.objects = loadedObjects;
}

- (void)dealloc {
    [self save];
}

// for reactive cocoa, didn't check working or not
- (void)setObject:(id)object forKey:(NSString *)key {
    if (object == nil || key == nil) return;
    
    [self.objects setObject:object forKey:key];
    [self save];
}



@end
