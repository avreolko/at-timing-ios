//
//  Cache.h
//  Transport
//
//  Created by Valentin Cherepyanko on 19/11/15.
//  Copyright © 2015 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSUserDefaults+RMSaveCustomObject.h"

#define CACHE_KEY @"AT_TIMING_CACHE"
#define LOGIN_CACHE_KEY @"loginCache"

#define NOTIF_TIME_KEY @"notificationsTime"
#define NOTIF_SHOW_KEY @"notificationsShow"

#define LOG_TYPE_KEY @"logTypeKey"
#define TICKETS_KEY @"tickets"

#define SELECTED_PRODUCT_KEY @"selectedProduct"

@interface Cache : NSObject

@property NSMutableDictionary *objects;

@property NSString *transportRefreshToken;
@property NSString *stopsRefreshToken;

+ (Cache *)get;
- (void)save;
- (void)load;
- (void)setObject:(id)object forKey:(NSString *)key;

@end
