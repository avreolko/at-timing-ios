//
//  AnimatedViewController.m
//  Conf
//
//  Created by Valentin Cherepyanko on 31/08/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import "UIView+Animations.h"

@implementation UIView (Animations)

- (void)hide {
    self.alpha = 0;
}

- (void)show {
    self.alpha = 1;
}

- (void)hideAnimated {
    if (self.alpha > 0) {
        self.alpha = 1;
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 0;
        } completion:nil];
    }
}

- (void)showAnimated {
    if (self.alpha < 1) {
        self.alpha = 0;
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 1;
        } completion:nil];
    }
}

- (void)hideAnimatedWithCompletion:(void (^)())callback {
    self.alpha = 1;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL completed) {
        callback();
    }];
}

- (void)showAnimatedWithCompletion:(void (^)())callback {
    self.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    } completion:^(BOOL completed) {
        callback();
    }];
}

- (void)popup {
    POPSpringAnimation *anim = [AnimationHelper popupAnimation];
    [self pop_addAnimation:anim forKey:@"popup"];
}

- (void)popdown {
    POPSpringAnimation *anim = [AnimationHelper popdownAnimation];
    [self pop_addAnimation:anim forKey:@"popdown"];
}

- (void)move:(CGPoint)point {
    [UIView animateWithDuration:0.5 animations:^{
        self.frame = CGRectOffset(self.frame, point.x, point.y);
    }];
}

- (void)addBlur {
    UIBlurEffect *effect = [AdjustableBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *beView = [[UIVisualEffectView alloc] initWithEffect:effect];
    beView.frame = self.bounds;
    
    self.backgroundColor = [UIColor clearColor];
    [self insertSubview:beView atIndex:0];
}

- (void)bounce {
    POPSpringAnimation *anim = [AnimationHelper bounceAnimation];
    [self pop_addAnimation:anim forKey:@"bounce"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
