//
//  Worklog.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UPDATE_LOCAL_WORKLOG_EVENT @"updateLogs"
#define ADD_LOCAL_LOG_EVENT @"addLocalLog"

@interface Worklog : NSObject

//{
//    "entitySpecId": "317d30ea-8fcd-7f78-82ee-208cc219f16d",
//    "project.code": "2017_Новосибирская_Smart-people внедрение",
//    "employee.login": "vcherepyanko",
//    "changeDate": "28.04.2017 08:46:43",
//    "entityId": "317d30ea-8fcd-7f78-82ee-208cc219f16d",
//    "logid": "102881",
//    "comment": "",
//    "value": "10800.0",
//    "objectId": "ca9de6ee-38f9-4d65-b6d8-ac639c5e2f44"
//}

@property NSString *id;
@property NSString *login;
@property NSNumber *minutes;
@property NSString *comment;
@property NSDate *date;
@property NSString *projectCode;
@property NSString *projectId;
@property NSString *ticket;
@property NSString *logid;
@property NSString *ticketName;
@property NSString *status;
@property NSString *poFIO;
@property NSString *declineReason;

+ (Worklog *)initWithArray:(NSArray *)array;
- (BOOL)canBeEdited;
- (BOOL)isApproved;
- (BOOL)isDeclined;
- (BOOL)isClosed;

@end
