//
//  Profile.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 18/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "Profile.h"
#import <objc/runtime.h>

@implementation Profile

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"employeeID": @"objectId",
                                                                  @"jobTitle": @"jobTitle.name"
                                                                  }];
}

- (NSMutableDictionary *)profileDict {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:self.login forKey:@"Логин"];
    [dict setObject:self.fio forKey:@"ФИО"];
    if (self.jobTitle) [dict setObject:self.jobTitle forKey:@"Должность"];
    
    return dict;
}

@end
