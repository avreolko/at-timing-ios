//
//  JiraTicket.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 18/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface JiraTicket : JSONModel

@property NSString *ticketTitle;
@property NSString *ticketCode;

- (NSString *)description;

@end
