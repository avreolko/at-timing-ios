//
//  SpecialDay.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialDay : NSObject

@property NSDate *date;
@property BOOL isHoliday;

@end
