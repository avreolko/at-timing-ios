//
//  WorklogCalendarDay.h
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 12/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WorklogCalendarDay : NSObject

@property NSNumber *minutes;
@property NSMutableDictionary *additionalData;

@end
