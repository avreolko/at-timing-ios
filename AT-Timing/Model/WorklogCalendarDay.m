//
//  WorklogCalendarDay.m
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 12/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "WorklogCalendarDay.h"

@implementation WorklogCalendarDay

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        self.additionalData = [NSMutableDictionary new];
    }
    
    return self;
}

@end
