//
//  BriefProjectInfo.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "BriefProjectInfo.h"

@implementation BriefProjectInfo

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.code forKey:@"code"];
    [aCoder encodeObject:self.objectId forKey:@"objectId"];
    [aCoder encodeObject:self.changeDate forKey:@"changeDate"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.code = [aDecoder decodeObjectForKey:@"code"];
        self.objectId = [aDecoder decodeObjectForKey:@"objectId"];
        self.changeDate = [aDecoder decodeObjectForKey:@"changeDate"];
    }
    return self;
}

@end
