//
//  Worklog.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "Worklog.h"

@implementation Worklog

//{
//    "entitySpecId": "317d30ea-8fcd-7f78-82ee-208cc219f16d",
//    "project.code": "2016_Бурятия_Smart-people внедрение",
//    "employee.login": "vcherepyanko",
//    "ticket.projectCode": "DEM-1185",
//    "changeDate": "28.04.2017 08:46:55",
//    "entityId": "317d30ea-8fcd-7f78-82ee-208cc219f16d",
//    "logid": "102882",
//    "comment": "",
//    "value": "3600.0",
//    "objectId": "5f524b76-6def-41a0-997f-c6c1d0c40cb5"
//}

//@property NSString *id;
//@property NSString *login;
//@property NSNumber *minutes;
//@property NSString *comment;
//@property NSDate *date;
//@property NSString *projectCode;
//@property NSString *projectId;
//@property NSString *ticket;

//+ (JSONKeyMapper *)keyMapper {
//    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"id": @"objectId",
//                                                                   @"ticket": @"ticket.projectCode",
//                                                                   @"projectCode": @"project.code",
//                                                                   @"projectCode": @"project.code",}];
//}

+ (Worklog *)initWithArray:(NSArray *)array {
    Worklog *log = [Worklog new];
    log.id = [array objectAtIndex:0];
    log.login = [array objectAtIndex:1];
    NSNumber *seconds = [array objectAtIndex:2];
    log.minutes = [NSNumber numberWithLong:(seconds.longValue / 60)];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];

    NSString *dateString = [self dateStringFrom:[array objectAtIndex:3]];

    log.date = [dateFormatter dateFromString:dateString];
    
    if (!log.date) {
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        log.date = [dateFormatter dateFromString:[array objectAtIndex:3]];
    }
    
    log.projectCode = [array objectAtIndex:4];
    log.comment = [array objectAtIndex:5];
    if ([[array objectAtIndex:6] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dict = [array objectAtIndex:6];
        log.projectId = [dict objectForKey:@"objectId"];
    }
    
    log.ticket = [array objectAtIndex:7];
    log.ticketName = [array objectAtIndex:8];
    log.status = [array objectAtIndex:9];

    if ([log.status isEqualToString:@"declined"]) {
        log.poFIO = [array objectAtIndex:10];
        log.declineReason = [array objectAtIndex:11];
    }

    log.logid = [array objectAtIndex:12];
//    if ((long)log.date.timeIntervalSince1970 % 19 == 0) {
//    if (arc4random_uniform(20) % 19 == 0) {
//        log.status = @"closed";
//    }
//
//    if (log.status.length > 0) {
//        NSLog(@"kek");
//    }
    
    return log;
}

- (BOOL)canBeEdited {
    return ![self.status isEqualToString:@"closed"];
}

- (BOOL)isApproved {
    return [self.status isEqualToString:@"approved"];
}
- (BOOL)isDeclined {
    return [self.status isEqualToString:@"declined"];
}
- (BOOL)isClosed {
    return [self.status isEqualToString:@"closed"];
}

+ (NSString *)dateStringFrom:(NSObject *)some {
    if ([some isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dateDictionary = (NSDictionary *)some;

        NSString *dateString = [dateDictionary objectForKey:@"text"];

        return dateString;
    }

    if ([some isKindOfClass:[NSString class]]) {
        NSString *dateString = (NSString *)some;
        return dateString;
    }

    assert(false);

    return nil;
}

@end
