//
//  JiraTicket.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 18/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "JiraTicket.h"

@implementation JiraTicket

- (NSString *)description {
    return [NSString stringWithFormat:@"[%@] %@", self.ticketCode, self.ticketTitle];
}

@end
