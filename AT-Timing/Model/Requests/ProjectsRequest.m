//
//  ProjectsRequest.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "ProjectsRequest.h"
#import "ProjectSearchResponse.h"

@implementation ProjectsRequest

- (instancetype)init {
    if ((self = [super init])) {
        self.limit = 10000;
        self.attributes = @[@"code", @"needToComment"].mutableCopy;
        self.entityId = @"3072c293-dc52-79e4-ca9c-4f1b37a2d880";
        self.search = @"";
        self.sort = @"code";
        self.sortAsc = YES;
        self.dataCondition = @"hideMobile != true";
        self.useCondition = YES;
    }
    
    return self;
}

- (void)loadWithSearch:(NSString *)search completion:(void (^)(NSArray *))callback {
    self.search = [search stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    [self load:callback];
}

- (void)load:(void (^)(NSArray *))callback {
    [self post:^(NiceResponse *response) {
        NSArray *projectsDataBundles = [EntitySearchResponse JSONArrayFromKeylessJSON:response.dataString];
        NSMutableArray *projects = [NSMutableArray new];
        
        for (NSArray *projectDataBundle in projectsDataBundles) {
            ProjectSearchResponse *project = [ProjectSearchResponse new];
            project.objectId = projectDataBundle[0];
            project.code = projectDataBundle[1];
            project.entityId = ProjectID;

			if ([projectDataBundle count] > 2) {
				project.needComment = projectDataBundle[2];
			}

            [projects addObject:project];
        }
        
        callback(projects.copy);
    }];
}

@end
