//
//  CalendarHolidaysRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 02/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "EntityRequest.h"

@interface CalendarHolidaysRequest : EntityRequest
@end
