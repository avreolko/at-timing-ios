//
//  SubjectRequest.h
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 12/05/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "EntityRequest.h"

@interface SubjectRequest : EntityRequest

@end
