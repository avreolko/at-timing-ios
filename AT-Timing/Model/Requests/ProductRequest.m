//
//  ProductRequest.m
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 13/05/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "ProductRequest.h"

@implementation ProductRequest

- (instancetype)init {
    if ((self = [super init])) {
        self.limit = 10000;
        self.attributes = @[@"name"].mutableCopy;
        self.entityId = @"907c9315-5655-dcb8-61f0-18ffc41bcf91";
        self.search = @"";
        self.sort = @"name";
        self.sortAsc = YES;
        self.dataCondition = @"hideMobile != true";
        self.useCondition = YES;
    }
    
    return self;
}

- (void)load:(void (^)(NSArray *))callback {
    [self post:^(NiceResponse *response) {
        NSArray *subjects = [EntitySearchResponse arrayOfModelsFromKeylessJSON:response.dataString keys:self.attributes];
        callback(subjects);
    }];
}

@end
