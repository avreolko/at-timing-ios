//
//  CalendarHolidaysRequest.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 02/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "CalendarHolidaysRequest.h"
#import "NSDate+Utilities.h"
#import "Utils.h"
#import "SpecialDay.h"

@implementation CalendarHolidaysRequest

- (instancetype)init {
    if ((self = [super init])) {
        self.limit = 10000;
        self.attributes = @[@"specialDay", @"isWorking"].mutableCopy;
        self.entityId = @"1be75d5e-da11-72fd-d2b4-e0fa4163686f";
        self.search = @"";
        self.dataCondition = [NSString stringWithFormat:@"Year == %ld", (long)[NSDate date].year];
        self.sort = @"specialDay";
        self.sortAsc = YES;
        self.useCondition = YES;
    }
    
    return self;
}

- (void)load:(void (^)(NSArray *))callback {
    [self post:^(NiceResponse *response) {
        NSArray *holidaysDataBundles = [EntitySearchResponse JSONArrayFromKeylessJSON:response.dataString];
        NSMutableArray *specialDays = [NSMutableArray new];
        
        for (NSArray *holidayDataBundle in holidaysDataBundles) {
            NSNumber *isHoliday = [holidayDataBundle objectAtIndex:2];

            NSString *dateString = [self dateStringFrom:[holidayDataBundle objectAtIndex:1]];

            NSDate *date = [Utils getDateFromString:dateString withFormat:@"dd.MM.yyyy"];
            SpecialDay *day = [SpecialDay new];
            day.isHoliday = isHoliday.boolValue;
            day.date = date;
            
            if (date != nil) [specialDays addObject:day];
        }
        
        callback(specialDays.copy);
    }];
}

- (NSString *)dateStringFrom:(NSObject *)some {
    if ([some isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dateDictionary = (NSDictionary *)some;

        NSString *dateString = [dateDictionary objectForKey:@"text"];

        return dateString;
    }

    if ([some isKindOfClass:[NSString class]]) {
        NSString *dateString = (NSString *)some;
        return dateString;
    }

    assert(false);

    return nil;
}

@end
