//
//  ProjectsRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "EntityRequest.h"

@interface ProjectsRequest : EntityRequest

- (void)loadWithSearch:(NSString *)search completion:(void (^)(NSArray *))callback;

@end
