//
//  SubproductRequest.h
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 13/05/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "EntityRequest.h"

@interface SubproductRequest : EntityRequest
- (instancetype)initWithProductID:(NSString *)productID;
@end
