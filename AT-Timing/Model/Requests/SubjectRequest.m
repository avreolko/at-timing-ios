//
//  SubjectRequest.m
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 12/05/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "SubjectRequest.h"

@implementation SubjectRequest

- (instancetype)init {
    if ((self = [super init])) {
        self.limit = 10000;
        self.attributes = @[@"name"].mutableCopy;
        self.entityId = @"9cd7150a-0f30-2a00-3051-07d2308b8ce5";
        self.search = @"";
        self.sort = @"name";
        self.sortAsc = YES;
        self.dataCondition = @"hideMobile != true";
        self.useCondition = YES;
    }
    
    return self;
}



@end
