//
//  SubproductRequest.m
//  AT-Timing Prod
//
//  Created by Valentin Cherepyanko on 13/05/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "SubproductRequest.h"

@implementation SubproductRequest

- (instancetype)init {
    if ((self = [super init])) {
        self.limit = 10000;
        self.attributes = @[@"name"].mutableCopy;
        self.entityId = @"c01f336a-100d-8ad7-480f-1564173765bb";
        self.search = @"";
        self.sort = @"name";
        self.sortAsc = YES;
        self.dataCondition = @"hideMobile != true";
        self.useCondition = YES;
    }
    
    return self;
}

- (instancetype)initWithProductID:(NSString *)productID {
    self = [self init];
    if (!productID || productID.length == 0) {
        return self;
    }
    
    self.useCondition = YES;
    self.bindType = @"entity";
    
    if (self.dataCondition.length == 0) {
        self.dataCondition = [NSString stringWithFormat:@"direction == '%@'", productID];
    } else {
        NSString *directionCondition = [NSString stringWithFormat:@"direction == '%@'", productID];
        self.dataCondition = [NSString stringWithFormat:@"%@ AND %@", self.dataCondition, directionCondition];
    }
    
    return self;
}

@end
