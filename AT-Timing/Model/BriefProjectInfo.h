//
//  BriefProjectInfo.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BriefProjectInfo : NSObject <NSCoding>

@property NSString *code;
@property NSString *objectId;
@property NSDate *changeDate;

@property NSString *ticket;
@property NSString *ticketName;

@end
