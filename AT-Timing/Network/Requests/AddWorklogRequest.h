//
//  AddWorklogRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 24/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface AddWorklogRequest : JSONModel

@property NSString *value;
@property NSString *changeDate;
@property NSString *startDate;
@property NSString *employee;
@property NSString *ticket;
@property NSString *project;
@property NSString *comment;
@property NSString *entityId;
@property NSString *objectId;
@property NSString *statusTime;
@property BOOL isNewItem;

//{
//    "value": "20",
//    "changeDate": "23.03.2017 10:31",
//    "employee": "5a127166-356b-ee2f-ac18-bce15d49e875",
//    "ticket": "",
//    "project": "e23f061c-37c6-69cf-5ef2-187efd93ee97",
//    "comment": "уввв",
//    "entityId": "317d30ea-8fcd-7f78-82ee-208cc219f16d",
//    "objectId": "30cf2ce4-4d4c-50f7-6f8c-04b308ea95d5",
//    "isNewItem": true
//}

//{"value":"20","changeDate":"23.03.2017 10:31","employee":"5a127166-356b-ee2f-ac18-bce15d49e875","ticket":"","project":"e23f061c-37c6-69cf-5ef2-187efd93ee97","comment":"уввв","entityId":"317d30ea-8fcd-7f78-82ee-208cc219f16d","objectId":"30cf2ce4-4d4c-50f7-6f8c-04b308ea95d5","isNewItem":true}

@end
