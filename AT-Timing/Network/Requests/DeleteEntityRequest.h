//
//  DeleteEntityRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 28/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface DeleteEntityRequest : JSONModel

@property NSMutableArray *objectIds;

@end
