//
//  JiraLogRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 16/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface JiraLogRequest : JSONModel

//"comment":"Some comment",
//"started":"2017-05-05T16:47:52.118+0000",
//"timeSpentSeconds":60

@property NSString *comment;
@property NSString *started;
@property int timeSpentSeconds;

@end
