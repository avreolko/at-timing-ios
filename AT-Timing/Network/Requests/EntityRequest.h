//
//  EntityRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "ServerRequestManager.h"
#import "EntitySearchResponse.h"

@protocol Request
- (void)get:(void (^)(NiceResponse *response))callback;
- (void)post:(void (^)(NiceResponse *response))callback;
@end

#define DEFAULT_LIMIT 10000
#define DEFAULT_SORT_ASC YES

@interface EntityRequest : JSONModel<Request>

@property NSString *entityId;
@property NSMutableArray *attributes;
@property NSString *search;
@property int limit;
@property NSMutableDictionary *facets;
@property NSString *sort;
@property BOOL sortAsc;
@property BOOL useCondition;
@property NSString *dataCondition;
@property NSString *bindType;

- (instancetype)initWithDefaultParameters;
- (void)load:(void (^)(NSArray *))callback;


- (void)setDatePastMonth;
- (void)addPreviousMonthCondition;

@end
