//
//  NewProjectRequest.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface NewProjectRequest : JSONModel

//{
//    "rules": "",
//    "description": "",
//    "code": "2017_Саратов_SmartCenter",
//    "year": "2017",
//    "product": "4e29420e-796b-40c7-328a-3e65ec62b487",
//    "subject": "36b7a6a1-19e5-1a00-a67e-bc7491a09212",
//    "entityId": "3072c293-dc52-79e4-ca9c-4f1b37a2d880",
//    "objectId": "b1d4f696-364c-da04-1e12-692d2c96f3ba",
//    "isNewItem": true
//}

@property NSString *code;
@property NSString *year;
@property NSString *product;
@property NSString *subject;
@property NSString *entityId;
@property NSString *objectId;
@property NSString *direction;
@property BOOL isNewItem;

@end
