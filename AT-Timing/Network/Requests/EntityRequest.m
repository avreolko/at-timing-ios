//
//  EntityRequest.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "EntityRequest.h"
#import "Utils.h"
#import "NSDate+Utilities.h"
#import "AuthManager.h"

@implementation EntityRequest

- (instancetype)initWithDefaultParameters {
    if ((self = [super init])) {
        self.limit = DEFAULT_LIMIT;
        self.sortAsc = DEFAULT_SORT_ASC;
    }
    return self;
}

- (void)setDatePastMonth {
    NSDate *date1 = [NSDate date];
    NSDateComponents *dateComponents1 = [[NSDateComponents alloc] init];
    [dateComponents1 setMonth:-1];
    date1 = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents1 toDate:date1 options:0];
    
    NSDate *date2 = [NSDate date];
    NSDateComponents *dateComponents2 = [[NSDateComponents alloc] init];
    [dateComponents2 setDay:1];
    date2 = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents2 toDate:date2 options:0];
    
    self.facets = [NSMutableDictionary new];
    [self.facets setObject:@[[Utils stringFromDates:date1 second:date2]] forKey:@"changeDate"];
}

- (void)addPreviousMonthCondition {
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSDate *arbitraryDate = [NSDate date];
//    NSDateComponents *comp = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:arbitraryDate];
//    [comp setMonth:arbitraryDate.month - 1];
//    [comp setDay:1];
//    NSDate *startDate = [gregorian dateFromComponents:comp];

    NSDate *endDate = [NSDate date];
    NSDateComponents *dateComponents2 = [[NSDateComponents alloc] init];
    [dateComponents2 setDay:1];
    endDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents2 toDate:endDate options:0];
    
    NSString *login = [AuthManager get].profile.login;
    NSString *startString = [AuthManager get].profile.worklogDateString;
    NSString *endString = [Utils getStringFromDate:endDate withFormat:@"dd.MM.yyyy"];
    
    self.useCondition = YES;
	self.dataCondition = [NSString stringWithFormat:@"employee==$employee AND startDate > %@ AND startDate < %@", startString, endString];
}

- (void)get:(void (^)(NiceResponse *response))callback {
    
}

- (void)post:(void (^)(NiceResponse *response))callback {
    NSURL *url = [ServerSettings entityURL];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:self.toJSONString completion:^(NiceResponse *response) {
        callback(response);
    }];
}

- (void)load:(void (^)(NSArray *))callback {
    [self post:^(NiceResponse *response) {
        NSArray *subjects = [EntitySearchResponse arrayOfModelsFromKeylessJSON:response.dataString keys:self.attributes];
        callback(subjects);
    }];
}

@end
