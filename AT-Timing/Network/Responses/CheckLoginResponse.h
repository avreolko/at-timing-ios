//
//  CheckLoginResponse.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <Foundation/Foundation.h>

@interface CheckLoginResponse : JSONModel

//[{
//    "jobTitle.name": "Тимлид",
//    "entitySpecId": "0d4363dd-41fe-1248-b76d-e9b3bafadddf",
//    "entityId": "0d4363dd-41fe-1248-b76d-e9b3bafadddf",
//    "login": "mpetrischev",
//    "fio": "Петрищев Максим Валерьевич",
//    "objectId": "5a127166-356b-ee2f-ac18-bce15d49e875"
//}]

@property NSString *entitySpecId;
@property NSString *entityId;
@property NSString *login;
@property NSString *objectId;
@property NSString *fio;
@property NSString *jobTitle;

@end
