//
//  SubjectSearchResponse.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "EntitySearchResponse.h"

@implementation EntitySearchResponse

+ (NSArray *)JSONArrayFromKeylessJSON:(NSString *)json {
    NSError *err = nil;
    NSData *dataString = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *array = [NSJSONSerialization JSONObjectWithData:dataString options:0 error:&err];
    if (!err) return array;
    else return nil;
}

+ (NSArray *)arrayOfModelsFromKeylessJSON:(NSString *)json keys:(NSArray *)keys {
    NSMutableArray *arrayOfModels = [NSMutableArray new];
    NSArray *arrays = [self JSONArrayFromKeylessJSON:json];
    for (NSArray *array in arrays) {
        EntitySearchResponse *response = [EntitySearchResponse new];
        response.objectId = [array objectAtIndex:0];
        response.name = [array objectAtIndex:1];
        response.entityId = @"";
        response.entitySpecId = @"";
        
        // не сработает универсальное решение, так как в ответе от сервера приходит в произвольном от attributes порядке
//        for (int i = 0; i < keys.count; i++) {
//            NSString *key = [keys objectAtIndex:i];
//            [response setValue:[array objectAtIndex:i] forKey:key];
//        }
        
        [arrayOfModels addObject:response];
    }
    
    return arrayOfModels;
}

@end
