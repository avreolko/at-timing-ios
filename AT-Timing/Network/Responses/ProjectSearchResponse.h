//
//  ProjectSearchResponse.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 23/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ProjectSearchResponse : JSONModel

@property NSString *entitySpecId;
@property NSString<Optional> *code;
@property NSString *entityId;
@property NSString *objectId;
@property BOOL needComment;

@end
