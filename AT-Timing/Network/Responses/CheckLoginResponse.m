//
//  CheckLoginResponse.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CheckLoginResponse.h"

@implementation CheckLoginResponse

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"jobTitle": @"jobTitle.name" }];
}

@end
