//
//  SubjectSearchResponse.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface EntitySearchResponse : JSONModel

@property NSString *entitySpecId;
@property NSString<Optional> *name;
@property NSString *entityId;
@property NSString *objectId;

//{
//    "entitySpecId": "9cd7150a-0f30-2a00-3051-07d2308b8ce5",
//    "deleted": false,
//    "name": "Омск",
//    "entityId": "9cd7150a-0f30-2a00-3051-07d2308b8ce5",
//    "defaultLabel": "",
//    "objectId": "efdeecee-53d2-15d6-9c2c-ce7dbee4713a",
//    "linked": false
//}

+ (NSArray *)JSONArrayFromKeylessJSON:(NSString *)json;
+ (NSArray *)arrayOfModelsFromKeylessJSON:(NSString *)json keys:(NSArray *)keys;

@end
