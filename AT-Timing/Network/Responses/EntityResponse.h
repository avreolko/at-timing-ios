//
//  EntityResponse.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface EntityResponse : JSONModel
@property NSArray *resultArrays;
@end
