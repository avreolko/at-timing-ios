//
//  JiraLogResponse.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface JiraLogResponse : JSONModel

@property NSString *objectId;
@property NSString *logId;

@end
