//
//  KeyboardEventsNotifier.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 02/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol KeyboardEventListener
@optional
- (void)keyboardWillAppear;
- (void)keyboardWillDisapper;
@end

@interface KeyboardEventsNotifier : NSObject

+ (instancetype)get;
- (void)addListener:(NSObject<KeyboardEventListener> *)listener;
- (void)removeListener:(NSObject<KeyboardEventListener> *)listener;

@property NSMutableArray<KeyboardEventListener> *listeners;
@property BOOL keyboardIsHere;

@end
