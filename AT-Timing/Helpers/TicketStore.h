//
//  TicketStore.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JiraTicket.h"

#define RECENT_TICKETS_CACHE_KEY @"recent tickets cache key"

@interface TicketStore : NSObject

@property NSMutableArray *recentTickets;

+ (TicketStore *)get;
- (void)addTicket:(JiraTicket *)ticket;
- (void)save;
- (void)addTestTickets;

@end
