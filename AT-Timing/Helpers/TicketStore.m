//
//  TicketStore.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "TicketStore.h"
#import "JiraTicket.h"
#import "Cache.h"

@implementation TicketStore

+ (TicketStore *)get {
    static TicketStore *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        [instance loadFromCache];
    });
    return instance;
}

- (void)loadFromCache {
    self.recentTickets = [NSMutableArray new];
    NSMutableArray *cachedTickets = [[Cache get].objects objectForKey:RECENT_TICKETS_CACHE_KEY];
    if (cachedTickets && cachedTickets.count > 0) self.recentTickets = cachedTickets;
}

- (void)addTicket:(JiraTicket *)ticket {
    if (![self checkTicketExistense:ticket]) [self.recentTickets addObject:ticket];
    [self save];
}

- (void)addTestTickets {
    if (self.recentTickets.count > 0) return;
    
    NSArray *ticketsTitles = @[@"Kek",
                               @"Tuk",
                               @"Lol",
                               @"HEH",
                               @"YEAH",
                               @"FFFF"];
    
    for (NSString *title in ticketsTitles) {
        JiraTicket *ticket = [JiraTicket new];
        ticket.ticketTitle = title;
        ticket.ticketCode = title;
        [self addTicket:ticket];
    }
}

- (BOOL)checkTicketExistense:(JiraTicket *)ticket {
    for (JiraTicket *savedTicket in self.recentTickets) {
        if ([savedTicket.ticketCode isEqualToString:ticket.ticketCode]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)save {
    [[Cache get].objects setObject:self.recentTickets forKey:RECENT_TICKETS_CACHE_KEY];
    [[Cache get] save];
}

@end
