//
//  AuthManager.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "AuthManager.h"
#import "EntityRequest.h"

@implementation AuthManager

+ (AuthManager *)get {
    static AuthManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.listeners = [NSMutableArray<AuthListener> new];
    });
    return instance;
}

- (void)login:(void (^)(NiceResponse *response))callback {
    NSURL *url = [ServerSettings authURL];
    
    NSString *authJson = @"";
    if ([url.absoluteString containsString:@"point"]) {
        authJson = @"{\"username\":\"mobile\",\"password\":\"mobile\"}";
    } else {
        authJson = @"{\"username\":\"mobile\",\"password\":\"mobile654321\"}";
    }
    
    [[ServerRequestManager new] POSTRequestWithURL:url andData:authJson completion:^(NiceResponse *response) {
        if (response.statusCode == 200) self.isAuthorized = YES;
        else NSLog(@"AUTH FAILED!");
        callback(response);
    }];
}

- (void)checkLogin:(NSString *)login completion:(void (^)(NiceResponse *response))callback {
    NSURL *url = [ServerSettings newCheckLoginUrl:login];

	EntityRequest *request = [EntityRequest new];
	request.attributes = @[@"login", @"fio", @"jobTitle.name", @"dateOfReception"].mutableCopy;
	request.dataCondition = @"login==$employeeLogin AND working==true";
	request.entityId = @"0d4363dd-41fe-1248-b76d-e9b3bafadddf";
	request.useCondition = YES;
	request.limit = 1;

	[[ServerRequestManager new] POSTRequestWithURL:url andData:request.toJSONString completion:^(NiceResponse *response) {

		if (response.statusCode == 200) {
			[self notifyListenersAboutSuccessfulAuth];
			callback(response);
		} else callback(nil);
	}];
}

//- (void)logout:(void (^)(NiceResponse *response))callback {
//    [[ServerRequestManager new] GETRequestWithURL:[ServerSettings log] completion:^(NiceResponse *response) {
//        if (response.code == 200) {
//            [self notifyListenersAboutLogout];
//            self.isAuthorized = NO;
//        }
//        callback(response);
//    }];
//}

- (void)addListener:(NSObject<AuthListener> *)listener {
    [self.listeners addObject:listener];
}

- (void)removeListener:(NSObject<AuthListener> *)listener {
    [self.listeners removeObject:listener];
}

- (void)notifyListenersAboutSuccessfulAuth {
    for (NSObject<AuthListener> *listener in self.listeners) {
        [listener authSuccess];
    }
}

- (void)notifyListenersAboutLogout {
    for (NSObject<AuthListener> *listener in self.listeners) {
        [listener logout];
    }
}

@end
