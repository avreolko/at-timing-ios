//
//  AuthManager.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequestManager.h"
#import "CheckLoginResponse.h"
#import "Profile.h"

@protocol AuthListener
@optional
- (void)authSuccess;
- (void)logout;
@end

@interface AuthManager : NSObject

@property BOOL isAuthorized;
@property NSString *login;
@property NSString *employeeID;

@property Profile *profile;

+ (AuthManager *)get;
- (void)login:(void (^)(NiceResponse *response))callback;
//- (void)logout:(void (^)(NiceResponse *response))callback;
- (void)checkLogin:(NSString *)login completion:(void (^)(NiceResponse *response))callback;
- (void)addListener:(NSObject<AuthListener> *)listener;
- (void)removeListener:(NSObject<AuthListener> *)listener;

@property NSMutableArray<AuthListener> *listeners;

@end
