//
//  RecentTicketsDelegate.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 31/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "RecentTicketsDelegate.h"
#import "TicketStore.h"
#import "Utils.h"

@implementation RecentTicketsDelegate

@synthesize tableView = _tableView;

+ (RecentTicketsDelegate *)get {
    static RecentTicketsDelegate *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    [_tableView reloadData];
}

- (UITableView *)tableView {
    return _tableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *recentTickets = [TicketStore get].recentTickets;
    return recentTickets.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recentTicketCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"recentTicketCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    JiraTicket *ticket = [[TicketStore get].recentTickets objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"[%@] %@", ticket.ticketCode, ticket.ticketTitle];;
    cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
    
    return cell;
}

//- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Удалить"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
//        [[TicketStore get].recentTickets removeObjectAtIndex:indexPath.row];
//        [[TicketStore get] save];
//    }];
//    deleteAction.backgroundColor = [Utils getColorFromInt:0xDF3636];
//    return @[deleteAction];
//}


@end
