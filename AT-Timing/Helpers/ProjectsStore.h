//
//  ProjectsStore.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 31/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewProjectRequest.h"
#import "ProjectSearchResponse.h"
#import "BriefProjectInfo.h"
#import "Cache.h"

#define RECENT_PROJECTS_CACHE_KEY @"recentProjectsCache"

@interface ProjectsStore : NSObject

@property NSMutableArray *recentProjects;
+ (ProjectsStore *)get;
- (void)addProjectWithRequest:(NewProjectRequest *)request;
- (void)addProjectWithResponse:(ProjectSearchResponse *)response;
- (void)addProject:(BriefProjectInfo *)project;
- (void)save;
@end
