//
//  HolidayChecker.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 01/06/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "HolidayChecker.h"
#import "CalendarHolidaysRequest.h"
#import "SpecialDay.h"

@implementation HolidayChecker

+ (instancetype)get {
    static HolidayChecker *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)loadData {
    [[CalendarHolidaysRequest new] load:^(NSArray *specialDays) {
        if (specialDays.count > 0) {
            self.specialDays = specialDays;
            _loaded = YES;
        }
    }];
}

- (BOOL)isThisHoliday:(NSDate *)date {
    if (!_loaded) {
        return [self isThisHolidayLocal: date];
    } else {
        for (SpecialDay *day in self.specialDays) {
            
            if (day.date.year == date.year &&
                day.date.month == date.month &&
                day.date.day == date.day &&
                day.isHoliday) {
                return YES;
            }
        }
        
        return NO;
    }
}

- (BOOL)isThisSpecialWorkingDay:(NSDate *)date {
    for (SpecialDay *day in self.specialDays) {
        
        if (day.date.year == date.year &&
            day.date.month == date.month &&
            day.date.day == date.day &&
            !day.isHoliday) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)isThisHolidayLocal:(NSDate *)date {
    
    NSInteger day = date.day;
    NSInteger month = date.month;
    
    if ((day == 1 && month == 1) ||
        (day == 2 && month == 1) ||
        (day == 3 && month == 1) ||
        (day == 4 && month == 1) ||
        (day == 5 && month == 1) ||
        (day == 7 && month == 1) ||
        (day == 23 && month == 2) ||
        (day == 8 && month == 3) ||
        (day == 1 && month == 5) ||
        (day == 9 && month == 5) ||
        (day == 12 && month == 6) ||
        (day == 4 && month == 11)) {
        return YES;
    }
    
    return NO;
}

- (void)authSuccess {
    [self loadData];
}

@end
