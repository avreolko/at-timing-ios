//
//  HolidayChecker.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 01/06/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDate+Utilities.h"
#import "AuthManager.h"

@interface HolidayChecker : NSObject <AuthListener>
+ (instancetype)get;
- (BOOL)isThisHoliday:(NSDate *)date;
- (BOOL)isThisSpecialWorkingDay:(NSDate *)date;

@property NSArray *specialDays;
@property BOOL loaded;

@end
