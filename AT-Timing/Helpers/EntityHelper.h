//
//  EntityHelper.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntityRequest.h"

@interface EntityHelper : NSObject

+ (EntityRequest *)getTimesheetRequest:(NSString *)username;
+ (NSMutableArray *)getWorklogsFromJson:(NSString *)json;

@end
