//
//  RecentTicketsDelegate.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 31/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RecentTicketsDelegate : NSObject <UITableViewDelegate, UITableViewDataSource>

@property UITableView *tableView;
+ (RecentTicketsDelegate *)get;

@end
