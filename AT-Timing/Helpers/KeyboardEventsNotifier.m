//
//  KeyboardEventsNotifier.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 02/06/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

#import "KeyboardEventsNotifier.h"

@implementation KeyboardEventsNotifier

+ (instancetype)get {
    static KeyboardEventsNotifier *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.listeners = [NSMutableArray<KeyboardEventListener> new];
        [instance subscribeForEvents];
    });
    return instance;
}

- (void)subscribeForEvents {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardShow) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardHide) name:UIKeyboardWillHideNotification object:nil];
}

- (void)onKeyboardShow {
    _keyboardIsHere = YES;
    [self notifyListenersAboutKeyboardShow];
}

- (void)onKeyboardHide {
    _keyboardIsHere = NO;
    [self notifyListenersAboutKeyboardHide];
}

- (void)notifyListenersAboutKeyboardShow {
    for (NSObject<KeyboardEventListener> *listener in self.listeners) {
        [listener keyboardWillAppear];
    }
}

- (void)notifyListenersAboutKeyboardHide {
    for (NSObject<KeyboardEventListener> *listener in self.listeners) {
        [listener keyboardWillDisapper];
    }
}

- (void)addListener:(NSObject<KeyboardEventListener> *)listener {
    if (![self.listeners containsObject:listener]) {
        [self.listeners addObject:listener];
    }
}

- (void)removeListener:(NSObject<KeyboardEventListener> *)listener {
    [self.listeners removeObject:listener];
}

@end
