//
//  ProjectsStore.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 31/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "ProjectsStore.h"

@implementation ProjectsStore

+ (ProjectsStore *)get {
    static ProjectsStore *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.recentProjects = [[Cache get].objects objectForKey:RECENT_PROJECTS_CACHE_KEY];
        if (instance.recentProjects == nil) instance.recentProjects = [NSMutableArray new];
    });
    return instance;
}

- (void)addProjectWithRequest:(NewProjectRequest *)request {
    BriefProjectInfo *project = [BriefProjectInfo new];
    project.code = request.code;
    project.objectId = request.objectId;
    
    [self addProject:project];
}

- (void)addProjectWithResponse:(ProjectSearchResponse *)response {
    BriefProjectInfo *project = [BriefProjectInfo new];
    project.code = response.code;
    project.objectId = response.objectId;
    
    [self addProject:project];
}

- (void)addProject:(BriefProjectInfo *)project {
    if ([self checkProjectExistense:project]) [self.recentProjects addObject:project];
    [self save];
}

- (BOOL)checkProjectExistense:(BriefProjectInfo *)project {
    NSArray *filteredArray = [self.recentProjects filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(BriefProjectInfo *object, NSDictionary *bindings) {
        return [object.objectId isEqualToString:project.objectId];;
    }]];
    
    return filteredArray.count == 0;
}

- (void)save {
    [[Cache get].objects setObject:self.recentProjects forKey:RECENT_PROJECTS_CACHE_KEY];
    [[Cache get] save];
}

@end
