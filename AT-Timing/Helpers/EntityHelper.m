//
//  EntityHelper.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 22/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "EntityHelper.h"
#import "Worklog.h"
#import "EntityResponse.h"
#import "ServerSettings.h"

@implementation EntityHelper

+ (EntityRequest *)getTimesheetRequest:(NSString *)username {
    EntityRequest *request = [EntityRequest new];
    request.entityId = TimesheetID;
    request.attributes = [NSMutableArray new];
    return request;
}

+ (NSMutableArray *)getWorklogsFromJson:(NSString *)json {
    NSError *err = nil;
//    NSArray *array = [EntityResponse arrayOfModelsFromString:json error:&err];
    NSArray *array = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
    
    NSMutableArray *logArray = [NSMutableArray new];
    for (NSArray *nestedArray in array) {
        Worklog *log = [Worklog initWithArray:nestedArray];
//        if ((arc4random_uniform(11) % 10) == 0) log.status = @"declined";
        [logArray addObject:log];
    }
    return logArray;
}

@end
