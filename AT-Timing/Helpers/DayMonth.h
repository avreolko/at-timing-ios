//
//  DayMonth.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 01/06/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DayMonth : NSObject

@property int day;

@end
