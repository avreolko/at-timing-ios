//
//  ModalCalendar.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 13/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar/FSCalendar.h>

@interface ModalCalendar : UIView

@property (weak, nonatomic) IBOutlet FSCalendar *calendar;

@property (weak, nonatomic) IBOutlet UIView *triangleView;

+ (ModalCalendar *)loadWithAnchorView:(UIView *)view andDelegate:(id)delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarHeightConstraint;

@end
