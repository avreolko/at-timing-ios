//
//  WorklogCell.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 24/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Worklog.h"

#define ADD_WORKLOG_FROM_CELL @"addWorklogFromCell"

@interface WorklogCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *project;
@property (weak, nonatomic) IBOutlet UILabel *comment;
@property (weak, nonatomic) IBOutlet UILabel *time;

- (void)configureWithLog:(Worklog *)log;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descrLabelHeight;

@property Worklog *log;

@end
