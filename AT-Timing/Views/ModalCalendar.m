//
//  ModalCalendar.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 13/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "ModalCalendar.h"

@implementation ModalCalendar

+ (ModalCalendar *)loadWithAnchorView:(UIView *)view andDelegate:(id)delegate {
    ModalCalendar *calendar = [[[NSBundle mainBundle] loadNibNamed:@"ModalCalendar" owner:self options:nil] objectAtIndex:0];
    
    CGPoint point = [view.superview convertPoint:view.frame.origin toView:nil];
    
    float degrees = 45; //the value in degrees
    calendar.triangleView.transform = CGAffineTransformMakeRotation(degrees * M_PI/180);
    
    calendar.calendar.delegate = delegate;
    calendar.calendar.dataSource = delegate;
    calendar.calendar.locale = [NSLocale localeWithLocaleIdentifier:@"ru-RU"];
    calendar.calendar.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase | FSCalendarCaseOptionsWeekdayUsesUpperCase;
    
    CGPoint viewOrigin = CGPointMake(point.x - calendar.frame.size.width + view.frame.size.width - 10, point.y + view.frame.size.height + 14);
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    calendar.frame = CGRectMake(viewOrigin.x, viewOrigin.y, calendar.frame.size.width, calendar.frame.size.height);
    
    return calendar;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 5;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 5;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.clipsToBounds = YES;
    self.layer.masksToBounds = NO;
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated {
    self.calendarHeightConstraint.constant = CGRectGetHeight(bounds);
    // Do other updates here
    [self layoutIfNeeded];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
