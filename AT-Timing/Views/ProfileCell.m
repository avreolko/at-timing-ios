//
//  ProfileCell.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 18/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWith:(NSString *)key andValue:(NSString *)value {
    _key.text = key;
    _value.text = value;
}

@end
