//
//  WorklogCell.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 24/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "WorklogCell.h"
#import "Utils.h"

@implementation WorklogCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithLog:(Worklog *)log {
    self.log = log;
    self.project.text = log.projectCode;
    self.comment.text = log.comment;
    if (log.comment.length == 0) self.comment.text = @"Нет комментария";
    self.time.text = [Utils hoursAndMinutesFromMinutes:[log.minutes longValue]];
    
    [self.time sizeToFit];
    [self.comment sizeToFit];
    //[self calculateHeight];
}

- (void)calculateHeight {
    CGFloat height = [Utils getHeightForLabel:self.comment];
    if (height == 0) height = 16;
    self.descrLabelHeight.constant = height;
    NSLog(@"%f", self.descrLabelHeight.constant);
}

- (IBAction)addNewWorklogWithProject:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:self.log forKey:@"worklog"];
    [[NSNotificationCenter defaultCenter] postNotificationName:ADD_WORKLOG_FROM_CELL object:self userInfo:dictionary];
}

@end
