//
//  CoolWorklogCell.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 04/04/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolWorklogCell.h"
#import "WorklogCell.h"

@implementation CoolWorklogCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 90;
    [self.tableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if(object == self.tableView && [keyPath isEqualToString:@"contentSize"]) {
        [self.tableView layoutIfNeeded];
        self.tableHeight.constant = self.tableView.contentSize.height;
    }
}

- (void)configureWithLogs:(NSMutableArray *)logs {
    _logs = logs;
    Worklog *log = _logs.firstObject;

    self.project.text = log.projectCode;

    if (log.ticket.length > 0 && NO == [log.logid containsString:@"OTRS"]) {
        self.project.text = [NSString stringWithFormat:@"[%@] %@", log.ticket, log.ticketName];
    }

    if ([log.logid containsString:@"OTRS"]) {
        self.project.lineBreakMode = NSLineBreakByWordWrapping;
        self.project.numberOfLines = 0;
        self.project.text = [NSString stringWithFormat:@"[%@] %@\n(%@)", log.logid, log.ticketName, log.projectCode];
    }
    
    long sumTime = 0;
    for (Worklog *log in logs) {
        sumTime += log.minutes.longValue;
    }
    self.sumTime.text = [Utils hoursAndMinutesFromMinutes:sumTime];
    
    [_tableView reloadData];
    [self.tableView layoutIfNeeded];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.logs.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"tinyWorklogCell";
    TinyWorklogCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        [self.tableView registerNib:[UINib nibWithNibName:@"TinyWorklogCell" bundle:nil] forCellReuseIdentifier:cellID];
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    }
    
    [cell configureWithLog:[self.logs objectAtIndex:indexPath.row]];
    
    if (_logs.count == indexPath.row + 1) {
        cell.separatorInset = UIEdgeInsetsMake(0, 999, 0, 0);
    } else {
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    return cell;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    Worklog *log = [self.logs objectAtIndex:indexPath.row];
    if (!log.canBeEdited) return @[];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:log forKey:@"worklog"];
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Править" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [[NSNotificationCenter defaultCenter] postNotificationName:EDIT_LOG_EVENT_CELL object:nil userInfo:dict];
    }];
    editAction.backgroundColor = [Utils getColorFromInt:MAIN_COLOR];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Удалить"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [[NSNotificationCenter defaultCenter] postNotificationName:DELETE_LOG_EVENT_CELL object:nil userInfo:dict];
    }];
    deleteAction.backgroundColor = [Utils getColorFromInt:ALERT_COLOR];
    
    return @[deleteAction,editAction];
}

- (IBAction)addNewWorklogWithProject:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:self.logs.firstObject forKey:@"worklog"];
    [[NSNotificationCenter defaultCenter] postNotificationName:ADD_WORKLOG_FROM_CELL object:self userInfo:dictionary];
}

- (void)dealloc {
    [self.tableView removeObserver:self forKeyPath:@"contentSize"];
}

@end
