//
//  AutocompleteField.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 15/08/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutoComlpeteView.h"
#import "ServerRequestManager.h"
#import "EntitySearchResponse.h"

@class AutocompleteField;
@protocol AutocompleteFieldDelegate
- (void)field:(AutocompleteField *)field selectedVariant:(NSString *)string;
@end

@interface AutocompleteField : UITextField <AutoCompleteViewDelegate>

@property IBOutlet UIButton *showAllVatiantsButton;
@property NSObject<AutocompleteFieldDelegate> *fieldDelegate;
@property IBInspectable NSString *entityID;
@property NSString *json;
@property AutoComlpeteView *acView;
@property NSArray *autocompleteObjects;
@property EntitySearchResponse *selectedObject;

@end
