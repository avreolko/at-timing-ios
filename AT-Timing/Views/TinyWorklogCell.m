//
//  TinyWorklogCell.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 04/04/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "TinyWorklogCell.h"
#import "UILabel+FormattedText.h"
#import "UIView+Animations.h"

@implementation TinyWorklogCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)addTicketLog:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:self.log forKey:@"worklog"];
    [[NSNotificationCenter defaultCenter] postNotificationName:ADD_TICKET_WORKLOG_FROM_CELL object:self userInfo:dictionary];
}

- (void)configureWithLog:(Worklog *)log {
    self.log = log;
    [self.addTicketLogButton hide];
    self.comment.text = log.comment;
    
    if (log.comment.length == 0) self.comment.text = @"Нет комментария";
    self.time.text = [Utils hoursAndMinutesFromMinutes:[log.minutes longValue]];
    
    [self.time sizeToFit];
    [self.comment sizeToFit];

    if (log.isDeclined && log.poFIO.length > 0 && log.declineReason.length > 0) {
        self.declineInfo.text = [NSString stringWithFormat:@"Отклонено (%@): %@", log.poFIO, log.declineReason];
        UIFont *boldFont = [UIFont fontWithName:@"OpenSans-SemiBold" size:14];
        [self.declineInfo setFont:boldFont beforeOccurenceOfString:@":"];
    } else {
        self.declineInfo.text = @"";
    }

    [self setBGColor];
}

- (void)setBGColor {
    if (self.log.isApproved) {
        self.backgroundColor = [Utils getColorFromInt:0xe4ffe7];
    } else if (self.log.isDeclined) {
        self.backgroundColor = [Utils getColorFromInt:0xffe4e4];
    } else if (self.log.isClosed) {
        self.backgroundColor = [Utils getColorFromInt:0xe9e9e9];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

@end
