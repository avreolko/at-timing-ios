//
//  ModalView.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 21/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "ModalView.h"

@implementation ModalView

- (void)appear {
    self.alpha = 0;
    [self pop_addAnimation:[AnimationHelper popupAnimation] forKey:@"popup"];
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1;
    }];
}

- (void)disappear {
    POPSpringAnimation *animation = [AnimationHelper popdownAnimation];
    animation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        [self removeFromSuperview];
    };
    [self pop_addAnimation:animation forKey:@"popdown"];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
