//
//  ProfileCell.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 18/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell

- (void)configureWith:(NSString *)key andValue:(NSString *)value;
@property (weak, nonatomic) IBOutlet UILabel *key;
@property (weak, nonatomic) IBOutlet UILabel *value;

@end
