//
//  CoolWorklogCell.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 04/04/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TinyWorklogCell.h"

#define DELETE_LOG_EVENT_CELL @"cellDeleteLogEvent"
#define EDIT_LOG_EVENT_CELL @"cellEditLogEvent"

@interface CoolWorklogCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *createLogButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet UILabel *project;
@property (weak, nonatomic) IBOutlet UILabel *sumTime;

@property NSMutableArray *logs;
- (void)configureWithLogs:(NSMutableArray *)logs;

@property UIView *greenLine;
@end
