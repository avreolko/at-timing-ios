//
//  AutocompleteField.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 15/08/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "AutocompleteField.h"

@implementation AutocompleteField

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addEvents];
}

- (void)addEvents {
    [self addTarget:self
                  action:@selector(textFieldDidChange)
        forControlEvents:UIControlEventEditingChanged];
    
    if (_showAllVatiantsButton) {
        [_showAllVatiantsButton addTarget:self
                 action:@selector(showAllVariants)
       forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)showAllVariants {
    if (!_acView) {
        [self getVariantsWithString:@""];
    } else {
        [_acView removeFromSuperview];
        _acView = nil;
    }
}

- (void)textFieldDidChange {
    self.selectedObject = nil;
    [self getVariantsWithString:self.text];
}

- (void)getVariantsWithString:(NSString *)searchString {
    // override this for getting variants from server or whatever
    
    if (_entityID.length == 0) return;
    
    NSURL *url = [ServerSettings entitySearchURL:self.entityID search:searchString];
    [[ServerRequestManager new] GETRequestWithURL:url completion:^(NiceResponse *response) {
        
        [self.acView removeFromSuperview];
        self.acView = nil;
        
        NSError *err = nil;
        _autocompleteObjects = [EntitySearchResponse arrayOfModelsFromString:response.dataString error:&err];
        
        NSMutableArray *autocompleteResults = [NSMutableArray new];
        for (EntitySearchResponse *response in _autocompleteObjects) {
            [autocompleteResults addObject:response.name];
        }
        
        if (autocompleteResults.count > 0) {
            self.acView = [AutoComlpeteView initWith:[autocompleteResults copy] andTextField:self andDelegate:self];
            [self.superview addSubview:self.acView];
        }
    }];
}

- (void)didSelectVariant:(NSUInteger)index {
    self.acView = nil;
    self.selectedObject = [_autocompleteObjects objectAtIndex:index];
    self.text = self.selectedObject.name;
    if ([self.fieldDelegate respondsToSelector:@selector(field:selectedVariant:)]) {
        [self.fieldDelegate field:self selectedVariant:self.text];
    }
}

- (void)showVariants {
    // override this for getting variants from server or whatever
}

@end
