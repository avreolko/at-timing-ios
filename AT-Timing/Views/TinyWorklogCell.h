//
//  TinyWorklogCell.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 04/04/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Worklog.h"
#import "Utils.h"
#import "AutoresizingButton.h"

#define ADD_TICKET_WORKLOG_FROM_CELL @"add ticket worklog from cell"

@interface TinyWorklogCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *comment;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet AutoresizingButton *addTicketLogButton;
@property Worklog *log;
@property (weak, nonatomic) IBOutlet UILabel *declineInfo;

- (void)configureWithLog:(Worklog *)log;
@end
