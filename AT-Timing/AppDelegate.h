//
//  AppDelegate.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

