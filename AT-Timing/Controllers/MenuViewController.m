//
//  MenuViewController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (IBAction)worklogsButtonTapped:(id)sender {
    [self presentThroughMenu:Timesheet];
}
    
- (IBAction)profileButtonTapped:(id)sender {
    [self presentThroughMenu:ProfileScreen];
}
    
- (IBAction)logoutButtonTapped:(id)sender {
    [[Cache get].objects removeObjectForKey:LOGIN_CACHE_KEY];
    [[Cache get] save];
    [AuthManager get].profile = nil;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [self presentThroughMenuStoryboard:AuthStoryboard];
}

- (IBAction)settingsButtonTapped:(id)sender {
    [self presentThroughMenu:Settings];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
