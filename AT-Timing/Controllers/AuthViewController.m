//
//  ViewController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "AuthViewController.h"
#import "AuthManager.h"
#import "CheckLoginResponse.h"

@interface AuthViewController ()

@end

@implementation AuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.errorImage hide];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.panelView hide];
    [self login];
}

- (void)login {
    self.loginTextField.text = @"";
    if ([AuthManager get].isAuthorized) {
        [self loginWithCache];
    } else {
        [LoadingBlocker showOn:self.view light:YES];
        [[AuthManager get] login:^(NiceResponse *response) {
            if (response.statusCode == 200) {
                [self loginWithCache];
                NSLog(@"AUTH COMPLETE!");
            } else {
                NSLog(@"AUTH FAILED! Retrying in 1 second...");
                [_timer invalidate];
                _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(login) userInfo:nil repeats:NO];
            }
        }];
    }
}

- (void)loginWithCache {
    NSString *cachedLogin = [[Cache get].objects objectForKey:LOGIN_CACHE_KEY];
    if (cachedLogin && cachedLogin.length > 0) {
        [self authWithLogin:cachedLogin];
    } else {
        [LoadingBlocker hideFrom:self.view];
        [self.panelView showAnimated];
    }
}

- (IBAction)loginButtonTapped:(id)sender {
    [self.loginTextField resignFirstResponder];
    
    NSString *login = self.loginTextField.text;
    
    if (![Utils isLatin:login]) {
        [self.errorImage showAnimated];
        return;
    } else {
        [self.errorImage hideAnimated];
    }
    
    if (login.length > 0) {
        NSString *login = [self.loginTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self authWithLogin:login];
    }
}

- (void)authWithLogin:(NSString *)login {
    if (self.panelView.alpha > 0) [LoadingBlocker showOn:self.view light:YES];
    [[AuthManager get] checkLogin:login completion:^(NiceResponse *response) {
        [LoadingBlocker hideFrom:self.view];
        NSError *err = nil;

		NSData *data = [response.dataString dataUsingEncoding:NSUTF8StringEncoding];
		NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];

		if (array.count == 0) {
			NSLog(@"Такого профиля нет!");
			[self.panelView showAnimated];
			[self presentViewController:[[AlertManager get] alertForStatus:LOGIN_INVALID] animated:YES completion:nil];
			return;
		}

		NSArray *profileArray = [array objectAtIndex:0];
		Profile *profile = [Profile new];
		profile.employeeID = [profileArray objectAtIndex:0];
		profile.login = [profileArray objectAtIndex:1];
		profile.fio = [profileArray objectAtIndex:2];
		profile.jobTitle = [profileArray objectAtIndex:3];
		NSDictionary *dict = [profileArray objectAtIndex:4];
		profile.worklogTimeStamp = (NSNumber *)[dict objectForKey:@"timestamp"];
		profile.worklogDateString = [dict objectForKey:@"text"];

        if (profile.login.length > 0) {
			[AuthManager get].profile = profile;
			[[Cache get] setObject:profile.login forKey:LOGIN_CACHE_KEY];
			NSLog(@"LOGIN IS VALID!");
			[self push:Timesheet];
        } else {
            [self.panelView showAnimated];
            [self presentViewController:[[AlertManager get] alertForStatus:LOGIN_INVALID] animated:YES completion:nil];
        }
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    _panelY.constant = -105;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    
//    [self.panelView move:CGPointMake(0, -105)];
//    [self.panelView moveOn]
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _panelY.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
//    [self.panelView move:CGPointMake(0, 105)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
