//
//  EditWorklogController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 28/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "EditWorklogController.h"
#import "AddWorklogRequest.h"
#import "AuthManager.h"
#import "EntityHelper.h"
#import "ProjectsStore.h"
#import "JiraLogRequest.h"
#import "TicketStore.h"
#import "ProjectsRequest.h"
#import "AutoComlpeteView.h"

@interface EditWorklogController ()

@end

@implementation EditWorklogController

@synthesize time = _time;

- (void)prepare {
    self.modalTransitionStyle = UIModalPresentationFormSheet;
    self.modalPresentationStyle = UIModalPresentationOverFullScreen;
}

- (void)viewWillAppear:(BOOL)animated {
    EditWorklogViewState state = [self getStateWith:_log];
    [self configureWith:state];
    
    if (_selectedProject) {
        [self expandWithProject];
    } else {
        self.projectField.text = self.log.projectCode;
		[self delayedProjectSearch];
    }
    
    self.panel.backgroundColor = [UIColor whiteColor];
    self.panel.layer.cornerRadius = 5;
    self.panel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.panel.layer.shadowOpacity = 0.2;
    self.panel.layer.shadowRadius = 5;
    self.panel.layer.shadowOffset = CGSizeMake(0, 0);
    self.panel.clipsToBounds = YES;
    self.panel.layer.masksToBounds = NO;
    
    [self.panel popup];
    [self fillForm];
}

- (EditWorklogViewState)getStateWith:(Worklog *)log {
    if (log == nil) {
        return newLog;
    }

    if (log.logid == nil || log.logid.length == 0 || [log.logid containsString:@"OTRS"]) {
        return editStandartLog;
    }

    if (log.logid != nil && log.logid.length > 0){
        return editJiraLog;
    }

    return unknown;
}

- (void)configureWith:(EditWorklogViewState)state {
    if (state == editStandartLog) {
        [_projectField addTarget:self
                          action:@selector(proceedProjectSearch)
                forControlEvents:UIControlEventEditingChanged];

        self.projectField.enabled = YES;
        self.infoButtonWidth.constant = 0;
    }

    if (state == editJiraLog) {
        self.projectField.enabled = NO;
        self.infoButtonWidth.constant = 30;
    }

    if (state == newLog) {
        self.projectField.enabled = NO;
        self.infoButtonWidth.constant = 0;
    }

    if (state == unknown) {
        assert(false);
    }
}

- (void)proceedProjectSearch {
    [self removeAutocompleteView];
    if (_projectSearchTimer) [_projectSearchTimer invalidate];
    _projectSearchTimer = [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(delayedProjectSearch) userInfo:nil repeats:NO];
}

- (void)delayedProjectSearch {

    [self.ac startAnimating];
    [[ProjectsRequest new] loadWithSearch:_projectField.text completion:^(NSArray *projects) {
        [self.ac stopAnimating];
        if (projects.count == 1) {
            ProjectSearchResponse *project = [projects objectAtIndex:0];
            if ([project.objectId isEqualToString:self.log.projectId]) {
                NSLog(@"Вернулся только один проект и он такой же!");
				self.selectedLoadedProject = project;
                return;
            }
        }

        self.projectSearchResults = projects;

        NSMutableArray *autocompleteResults = [NSMutableArray new];
        for (ProjectSearchResponse *response in _projectSearchResults) {
            if (response.code) [autocompleteResults addObject:response.code];
        }

        if (autocompleteResults.count > 0) {
            self.acView = [AutoComlpeteView initWith:[autocompleteResults copy] andTextField:_projectField andDelegate:self];
            UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
            CGPoint pointInWindowCoords = [mainWindow convertPoint:self.acView.frame.origin fromWindow:nil];
            CGPoint origin = [self.worklogView convertPoint:pointInWindowCoords toView:mainWindow];
            self.acView.frame = CGRectMake(origin.x, origin.y, self.acView.frame.size.width, self.acView.frame.size.height);
            [self.view addSubview:self.acView];
        }
    }];
}

- (void)didSelectVariant:(NSUInteger)index {
    self.selectedLoadedProject = [self.projectSearchResults objectAtIndex:index];
    _projectField.text = self.selectedLoadedProject.code;
    self.projectSearchResults = nil;
    [self removeAutocompleteView];
}

- (void)removeAutocompleteView {
    [self.acView hideAnimatedWithCompletion:^{
        [self.acView removeFromSuperview];
        self.acView = nil;
    }];
}

- (IBAction)infoButtonTapped:(id)sender {
    [_infoMessage hide];
    [_infoMessage showAnimated];
    _infoMessageHeight.constant = 46;
    [UIView animateWithDuration:0.5 animations:^{
        [self.panel layoutIfNeeded];
    }];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [_infoMessage hideAnimated];
        _infoMessageHeight.constant = 0;
        [UIView animateWithDuration:0.5 animations:^{
            [self.panel layoutIfNeeded];
        }];
    });
}

- (void)passData:(id)object {
    // edit
    if ([object class] == [Worklog class]) {
        self.log = (Worklog *)object;
    }
    
    // add
    if ([object class] == [BriefProjectInfo class]) {
        self.selectedProject = (BriefProjectInfo *)object;
    }
}

- (void)expandWithProject {
    if (_selectedProject.ticket.length > 0) self.projectField.text = _selectedProject.ticket;
    else self.projectField.text = self.selectedProject.code;
    
    [self.projectField showAnimated];
    [self.editButton setTitle:@"Добавить" forState:UIControlStateNormal];
    
    self.projectFieldHeight.constant = 30;
}

- (void)fillForm {
    self.time = [self.log.minutes longValue];
    self.comment.text = self.log.comment;
}

- (IBAction)addTime:(id)sender {
    UIButton *button = (UIButton *)sender;
    self.time += button.tag;
}

- (IBAction)clearTime:(id)sender {
    self.time = 0;
}

- (IBAction)editWorklog:(id)sender {
	if (self.selectedLoadedProject.needComment && self.comment.text.length < 4) {
		[self.comment setRed:YES];
		return;
	} else {
		[self.comment setRed:NO];
	}

	AddWorklogRequest *request = [AddWorklogRequest new];
    
    if (self.log.ticket.length > 0 && NO == [self.log.ticket containsString:@"OTRS"]) {
        [self editJiraLog];
        return;
    }
    
    if (self.selectedProject.ticket.length > 0) {
        [self addJiraLog];
        return;
    }
    
    // ну дичь пиздец блять
    if (self.log) {
        request.objectId = self.log.id;
        request.startDate = [Utils getChangedDateWithDate:self.log.date];
        request.changeDate = [Utils getChangedDateWithDate:[NSDate date]];
        request.isNewItem = NO;
    }

    if (self.selectedProject) {
        request.project = self.selectedProject.objectId;
        request.objectId = [[Utils generateUUID] lowercaseString];
        request.startDate = [Utils getChangedDateWithDate:self.selectedProject.changeDate];
        request.changeDate = [Utils getChangedDateWithDate:[NSDate date]];
        request.isNewItem = YES;
    }

    if (self.selectedLoadedProject) {
        request.project = self.selectedLoadedProject.objectId;
        request.isNewItem = NO;
    }
    
    request.employee = [AuthManager get].profile.employeeID;
    request.entityId = TimesheetID;
    request.comment = self.comment.text;
    request.value = [NSString stringWithFormat:@"%ld", self.time*60];
    request.statusTime = StatusTimeID;
    request.ticket = NULL;

    if (self.selectedLoadedProject != nil) {
        request.project = self.selectedLoadedProject.objectId;
    }
    
    NSURL *url = nil;
    if (self.log) url = [ServerSettings editEntityURL:self.log.id];
    else url = [ServerSettings editEntityURL:request.objectId];
    
    [LoadingBlocker showOn:self.worklogView light:YES];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:request.toJSONString completion:^(NiceResponse *response) {
        [LoadingBlocker hideFrom:self.worklogView];
        if (response.statusCode == 200) {
            NSLog(@"WORKLOG SUCCESS!");
            if (self.log) [self updateLocalLog];
            if (self.selectedProject) {
                [self addLocalLog:request.objectId];
                [[ProjectsStore get] addProject:self.selectedProject];
            }
            
            [self.panel popdown];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

- (void)addJiraLog {
    JiraLogRequest *request = [JiraLogRequest new];
    request.started = [Utils isoStringFromDate:self.selectedProject.changeDate];
    request.timeSpentSeconds = (int)self.time*60;
    request.comment = self.comment.text;
    
    NSURL *url = [ServerSettings jiraLogURL:self.selectedProject.ticket];
    NSString *datastring = request.toJSONString;
    
    [LoadingBlocker showOn:self.worklogView light:YES];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:datastring completion:^(NiceResponse *response) {
        [LoadingBlocker hideFrom:self.worklogView];
        if (response.statusCode == 201) {
            NSLog(@"JIRA WORKLOG SUCCESS!");
            [self addLocalLog:response.dataString];
            
            JiraTicket *ticket = [JiraTicket new];
            ticket.ticketCode = _selectedProject.ticket;
            ticket.ticketTitle = _selectedProject.ticketName;
            [[TicketStore get] addTicket:ticket];
            
            [self.panel popdown];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            NSLog(@"JIRA LOG STATUS: %ld", (long)response.statusCode);
        }
    }];
}

- (void)editJiraLog {
    JiraLogRequest *request = [JiraLogRequest new];
    request.started = [Utils isoStringFromDate:self.log.date];
    request.timeSpentSeconds = (int)self.time*60;
    request.comment = self.comment.text;
    
    NSURL *url = [ServerSettings editEntityURL:self.log.id];
    [LoadingBlocker showOn:self.worklogView light:YES];
    [[ServerRequestManager new] GETRequestWithURL:url completion:^(NiceResponse *response) {
        NSError *err = nil;
        //    NSArray *array = [EntityResponse arrayOfModelsFromString:json error:&err];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response.dataString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
        NSString *logid = [dict objectForKey:@"logid"];
        
        NSURL *editURL = [ServerSettings jiraEditLogURL:self.log.ticket logID:logid];
        NSString *dataString = request.toJSONString;
        [[ServerRequestManager new] PUTRequestWithURL:editURL andData:dataString completion:^(NiceResponse *response) {
            [LoadingBlocker hideFrom:self.worklogView];
            NSLog(@"JIRA EDIT RESPONSE CODE: %ld", (long)response.statusCode);
            if (self.log) [self updateLocalLog];
            [self.panel popdown];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    _panelY.constant = -80;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    //    [self.panelView move:CGPointMake(0, -105)];
    //    [self.panelView moveOn]
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _panelY.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    //    [self.panelView move:CGPointMake(0, 105)];
}

- (void)updateLocalLog {
    self.log.comment = _comment.text;
    self.log.minutes = [NSNumber numberWithLong:self.time];

    if (self.selectedLoadedProject) {
        self.log.projectId = self.selectedLoadedProject.objectId;
        self.log.projectCode = self.selectedLoadedProject.code;
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_LOCAL_WORKLOG_EVENT object:nil];
}

- (void)addLocalLog:(NSString *)logID {
    Worklog *log = [Worklog new];
    log.id = logID;
    log.login = [AuthManager get].profile.login;
    log.minutes = [NSNumber numberWithLong:self.time];
    log.comment = self.comment.text;
    log.date = self.selectedProject.changeDate;

    if (self.selectedProject != nil) {
        log.projectCode = self.selectedProject.code;
        log.projectId = self.selectedProject.objectId;
    }

    if (self.selectedLoadedProject != nil) {
        log.projectCode = self.selectedLoadedProject.code;
        log.projectId = self.selectedLoadedProject.objectId;
    }

    log.ticket = self.selectedProject.ticket;
    if (!log.ticket) log.ticket = @"";
    log.ticketName = @"";
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:log forKey:@"worklog"];
    [[NSNotificationCenter defaultCenter] postNotificationName:ADD_LOCAL_LOG_EVENT object:self userInfo:dictionary];
}

- (void)setTime:(long)time {
    _time = time;
    
    if (_time == 0) self.editButton.enabled = false;
    else self.editButton.enabled = true;
    
    [self calcTimeField];
}

- (long)time {
    return _time;
}

- (IBAction)close:(id)sender {
    [self.comment resignFirstResponder];
    [self.panel popdown];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)calcTimeField {
    _timeField.text = [Utils hoursAndMinutesFromMinutes:self.time];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
