//
//  EditWorklogController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 28/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"
#import "Worklog.h"
#import "BriefProjectInfo.h"

@protocol AutoCompleteViewDelegate;
@class AutoComlpeteView;
@class ProjectSearchResponse;

typedef enum EditWorklogViewState: NSUInteger {
    newLog,
    editJiraLog,
    editStandartLog,
    unknown
} EditWorklogViewState;

@interface EditWorklogController : CoolViewController <AutoCompleteViewDelegate>

@property Worklog *log;
@property BriefProjectInfo *selectedProject;
@property NSArray *projectSearchResults;
@property AutoComlpeteView *acView;
@property NSTimer *projectSearchTimer;
@property ProjectSearchResponse *selectedLoadedProject;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ac;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoMessageHeight;
@property (weak, nonatomic) IBOutlet UILabel *infoMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoButtonWidth;

@property (weak, nonatomic) IBOutlet UIView *panel;
@property (weak, nonatomic) IBOutlet UIView *worklogView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
- (IBAction)editWorklog:(id)sender;
@property (weak, nonatomic) IBOutlet ShapedButton *editButton;

@property (weak, nonatomic) IBOutlet CoolTextView *comment;
@property (weak, nonatomic) IBOutlet UITextField *timeField;
@property (weak, nonatomic) IBOutlet UITextField *projectField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *worklogViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *projectFieldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panelY;
@property long time;

@end
