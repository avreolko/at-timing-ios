//
//  AddProjectController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "AddProjectController.h"
#import "NewProjectRequest.h"
#import "ProjectsStore.h"
#import "EntityRequest.h"

@interface AddProjectController ()

@end


@implementation AddProjectController

@synthesize selectedSubject = _selectedSubject;
@synthesize selectedProduct = _selectedProduct;
@synthesize selectedSubproduct = _selectedSubproduct;

@synthesize allProducts = _allProducts;
@synthesize allSubjects = _allSubjects;
@synthesize allSubproducts = _allSubproducts;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self checkCache];
    // Do any additional setup after loading the view from its nib.
    
    self.selectedProduct = nil;
    [self setProjectCodeLabel];
    
    [_subjectField addTarget:self
                  action:@selector(proceedSubjectSearch)
        forControlEvents:UIControlEventEditingChanged];
    
    [_productField addTarget:self
                      action:@selector(proceedProductSearch)
            forControlEvents:UIControlEventEditingChanged];
    
    [_subproductField addTarget:self
                      action:@selector(proceedSubproductSearch)
            forControlEvents:UIControlEventEditingChanged];
    
    [self setProjectCodeLabel];
    [self loadAllProducts];
    [self loadAllSubjects];
    [self loadAllSubproducts];
    
    [self.subjectError hide];
    [self.productError hide];
    [self.subproductError hide];
}

- (void)checkCache {
    self.selectedProduct = [[Cache get].objects objectForKey:SELECTED_PRODUCT_KEY];
    if (self.selectedProduct && _selectedProduct.name.length > 0) {
        _productField.text = _selectedProduct.name;
    }
}

- (NSArray *)filterEntities:(NSArray *)entities search:(NSString *)searchString {
    if (entities.count == 0) {
        return nil;
    }
    
    return [entities filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(EntitySearchResponse *subject, NSDictionary *bindings) {
        NSString *name = subject.name.lowercaseString;
        NSString *search = searchString.lowercaseString;
        
        return ([name containsString:search]);
    }]];
}

- (void)proceedSubjectSearch {
    [self setProjectCodeLabel];
    self.selectedSubject = nil;
    [self.acView removeFromSuperview];
    self.acView = nil;
    _allSubjectsShown = NO;
    
    if (_subjectField.text.length == 0) { return; }
    NSArray *localSearchResults = [self filterEntities:self.allSubjects search:_subjectField.text];
    if (localSearchResults != nil) {
        _subjectSearchResults = localSearchResults;
        [self showAutocompleteWith:_subjectSearchResults anchor:self.subjectField];
        return;
    }
    
    _subjectsRequest = [SubjectRequest new];
    _subjectsRequest.search = _subjectField.text;
    [_subjectsRequest load:^(NSArray *subjects) {
        if (subjects && subjects.count > 0) {
            _subjectSearchResults = subjects;

            [self showAutocompleteWith:_subjectSearchResults anchor:self.subjectField];
        }

        _subjectsRequest = nil;
    }];
}

- (void)proceedProductSearch {
    [self setProjectCodeLabel];
    self.selectedProduct = nil;
    [self.acView removeFromSuperview];
    self.acView = nil;
    _allProductsShown = NO;
    
    if (_productField.text.length == 0) { return; }
    NSArray *localSearchResults = [self filterEntities:self.allProducts search:_productField.text];
    if (localSearchResults != nil) {
        _productSearchResults = localSearchResults;
        [self showAutocompleteWith:_productSearchResults anchor:self.productField];
        return;
    }
    
    _productsRequest = [ProductRequest new];
    _productsRequest.search = _productField.text;
    
    [_productsRequest load:^(NSArray *products) {
        if (products && products.count > 0) {
            _productSearchResults = products;

            [self showAutocompleteWith:_productSearchResults anchor:self.productField];
        }
        
        _productsRequest = nil;
    }];
}

- (void)proceedSubproductSearch {
    [self setProjectCodeLabel];
    self.selectedSubproduct = nil;
    [self.acView removeFromSuperview];
    self.acView = nil;
    _allSubproductsShown = NO;
    
    if (_subproductField.text.length == 0) { return; }
    NSArray *localSearchResults = [self filterEntities:self.allSubproducts search:_subproductField.text];
    if (localSearchResults != nil) {
        _subproductSearchResults = localSearchResults;
        [self showAutocompleteWith:_subproductSearchResults anchor:self.subproductField];
        return;
    }
}

- (void)showAutocompleteWith:(NSArray *)searchResults anchor:(UIView *)anchor {
    NSMutableArray *autocompleteResults = [NSMutableArray new];
    for (EntitySearchResponse *response in searchResults) {
        [autocompleteResults addObject:response.name];
    }
    
    if (autocompleteResults.count > 0) {
        self.acView = [AutoComlpeteView initWith:[autocompleteResults copy] andTextField:anchor andDelegate:self];
        [self.view addSubview:self.acView];
    }
}

- (void)didSelectVariant:(NSUInteger)index {
    if (_allSubjectsShown) {
        self.allSubjectsShown = NO;
        self.selectedSubject = [self.allSubjects objectAtIndex:index];
        self.subjectField.text = self.selectedSubject.name;
        [self setProjectCodeLabel];
        return;
    }
    
    if (_allProductsShown) {
        self.allProductsShown = NO;
        self.selectedProduct = [self.allProducts objectAtIndex:index];
        self.productField.text = self.selectedProduct.name;
        [self setProjectCodeLabel];
        return;
    }
    
    if (_allSubproductsShown) {
        self.allSubproductsShown = NO;
        self.selectedSubproduct = [self.allSubproducts objectAtIndex:index];
        self.subproductField.text = self.selectedSubproduct.name;
        [self setProjectCodeLabel];
        return;
    }
    
    if (_subjectField.isFirstResponder) {
        self.selectedSubject = [self.subjectSearchResults objectAtIndex:index];
        self.subjectField.text = self.selectedSubject.name;
        [self.productField becomeFirstResponder];
    } else if (_productField.isFirstResponder) {
        self.selectedProduct = [self.productSearchResults objectAtIndex:index];
        self.productField.text = self.selectedProduct.name;
        [self.subproductField becomeFirstResponder];
    } else if (_subproductField.isFirstResponder) {
        self.selectedSubproduct = [self.subproductSearchResults objectAtIndex:index];
        self.subproductField.text = self.selectedSubproduct.name;
    }
    
    [self setProjectCodeLabel];
}

- (void)setProjectCodeLabel {
    NSString *subject = _subjectField.text;
    NSString *product = _productField.text;
    NSString *subproduct = _subproductField.text;
    
    NSString *labelText = [NSString new];
    if (subject.length > 0) {
        labelText = [NSString stringWithFormat:@"%@", subject];
    }
    
    if (product.length > 0) {
        if (labelText.length > 0) labelText = [NSString stringWithFormat:@"%@_", labelText];
        labelText = [NSString stringWithFormat:@"%@%@", labelText, product];
    }
    
    if (subproduct.length > 0) {
        if (labelText.length > 0) labelText = [NSString stringWithFormat:@"%@_", labelText];
        labelText = [NSString stringWithFormat:@"%@%@", labelText, subproduct];
    }
    
//    if (subject.length == 0) subject = @"<Субъект>";
//    if (product.length == 0) product = @"<Продукт>";
//    if (subproduct.length == 0) subproduct = @"<Подпроект>";
    
    self.codeLabel.text = labelText;
}

- (void)field:(AutocompleteField *)field selectedVariant:(NSString *)string {
    [self setProjectCodeLabel];
}

- (IBAction)addProject:(id)sender {
    if (!self.selectedSubject) [self.subjectError showAnimated];
    else [self.subjectError hideAnimated];
    
    if (!self.selectedProduct) [self.productError showAnimated];
    else [self.productError hideAnimated];
    
    if (!self.selectedSubproduct) [self.subproductError showAnimated];
    else [self.subproductError hideAnimated];
    
    if ([self checkFields] && self.selectedSubject && self.selectedProduct && self.selectedSubproduct) {
        [self checkExistingProject:^(BOOL cool) {
            if (!cool) {
                [[AlertManager get] show:PROJECT_EXISTS in:self];
            } else {
                NewProjectRequest *request = [NewProjectRequest new];
                request.code = self.codeLabel.text;
                request.product = self.selectedSubproduct.objectId;
                request.subject = self.selectedSubject.objectId;
                request.direction = self.selectedProduct.objectId;
                request.entityId = ProjectID;
                request.objectId = [[Utils generateUUID] lowercaseString];
                request.isNewItem = YES;
                
                [LoadingBlocker showOn:self.view light:YES];
                NSURL *url = [ServerSettings addObjectURL:request.objectId];
                [[ServerRequestManager new] POSTRequestWithURL:url andData:request.toJSONString completion:^(NiceResponse *response) {
                    [LoadingBlocker hideFrom:self.view];
                    if (response.statusCode == 200) {
                        NSLog(@"PROJECT CREATION SUCCESS!");
                        [[ProjectsStore get] addProjectWithRequest:request];
                        [self.navigationController popViewControllerAnimated:YES];
                    } if (response.statusCode == 418) {
                        [[AlertManager get] show:PROJECT_EXISTS in:self];
                    }
                }];
            }
        }];
    }
}

#pragma mark - all fields results

- (void)loadAllSubjects {
    _allSubjects = [NSArray new];
    [_allSubjectsButton hide];
    [_subjectsAI startAnimating];
    
    _subjectsRequest = [SubjectRequest new];
    [_subjectsRequest load:^(NSArray *subjects) {
        [_allSubjectsButton showAnimated];
        [_subjectsAI stopAnimating];
        
        self.allSubjects = subjects;
        _subjectsRequest = nil;
    }];
}

- (void)loadAllProducts {
    _allProducts = [NSArray new];
    [_allProductsButton hide];
    [_productsAI startAnimating];
    
    _productsRequest = [ProductRequest new];
    [_productsRequest load:^(NSArray *products) {
        [_allProductsButton showAnimated];
        [_productsAI stopAnimating];
        
        self.allProducts = products;
        _productsRequest = nil;
    }];
}

- (void)loadAllSubproducts {
    _allSubproducts = [NSArray new];
    [_allSubproductsButton hide];
    [_subproductsAI startAnimating];
    _subproductsRequest = [[SubproductRequest alloc] initWithProductID:self.selectedProduct.objectId];
    [_subproductsRequest load:^(NSArray *subproducts) {
//        if (self.selectedProduct) [_allSubproductsButton showAnimated];
        [_subproductsAI stopAnimating];
        
        self.allSubproducts = subproducts;
        _subproductsRequest = nil;
    }];
}

- (void)checkExistingProject:(void (^)(BOOL cool))completion {
    EntityRequest *request = [EntityRequest new];
    request.entityId = ProjectID;
    request.attributes = [NSMutableArray new];
    [request.attributes addObject:@"code"];
    request.limit = 20;
    
    request.facets = [NSMutableDictionary new];
    [request.facets setObject:@[_productField.text] forKey:@"product.name"];
    [request.facets setObject:@[_subjectField.text] forKey:@"subject.name"];
    
    NSURL *url = [ServerSettings entityURL];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:request.toJSONString completion:^(NiceResponse *response) {
        NSError *err = nil;
        NSArray *array = [NSJSONSerialization JSONObjectWithData:[response.dataString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
        if (array.count > 0 || response.statusCode != 200) {
            completion(NO);
        } else {
            completion(YES);
        }
    }];
}

- (IBAction)showAllSubjects:(id)sender {
    [self.acView removeFromSuperview];
    self.acView = nil;
    
    self.allSubjectsShown = !self.allSubjectsShown;
    [_subjectField resignFirstResponder];
    if (self.allSubjectsShown) [self showAutocompleteWith:_allSubjects field:_subjectField];
}

- (IBAction)showAllProducts:(id)sender {
    [self.acView removeFromSuperview];
    self.acView = nil;
    
    self.allProductsShown = !self.allProductsShown;
    [_productField resignFirstResponder];
    if (self.allProductsShown) [self showAutocompleteWith:_allProducts field:_productField];
}

- (IBAction)showAllSubproducts:(id)sender {
    [self.acView removeFromSuperview];
    self.acView = nil;
    
    self.allSubproductsShown = !self.allSubproductsShown;
    [_subproductField resignFirstResponder];
    if (self.allSubproductsShown) [self showAutocompleteWith:_allSubproducts field:_subproductField];
}

- (void)showAutocompleteWith:(NSArray *)entitiesArray field:(UITextField *)field {
    NSMutableArray *autocompleteResults = [NSMutableArray new];
    for (EntitySearchResponse *response in entitiesArray) {
        [autocompleteResults addObject:response.name];
    }
    
    if (autocompleteResults.count > 0) {
        self.acView = [AutoComlpeteView initWith:[autocompleteResults copy] andTextField:field andDelegate:self];
        [self.view addSubview:self.acView];
    }
}

- (void)setSelectedSubject:(EntitySearchResponse *)selectedSubject {
    _selectedSubject = selectedSubject;
    
    if (selectedSubject == nil) [self.subjectError showAnimated];
    else [self.subjectError hideAnimated];
}
- (EntitySearchResponse *)selectedSubject {
    return _selectedSubject;
}

- (void)setSelectedProduct:(EntitySearchResponse *)selectedProduct {
    if (selectedProduct == nil) {
        [self.productError showAnimated];
        return;
    } else {
        [self.productError hideAnimated];
    }
    
    if(![_selectedProduct.objectId isEqualToString:selectedProduct.objectId]) {
        _selectedSubproduct = nil;
        _subproductField.text = @"";
    }
    
    _selectedProduct = selectedProduct;
    
    if (!selectedProduct) {
        _subproductField.enabled = NO;
        _subproductField.placeholder = @"Выберите продукт";
    } else {
        _subproductField.enabled = YES;
        _subproductField.placeholder = @"Подпродукт";
    }
    [self loadAllSubproducts];
}
- (EntitySearchResponse *)selectedProduct {
    return _selectedProduct;
}


- (void)setSelectedSubproduct:(EntitySearchResponse *)selectedSubproduct {
    _selectedSubproduct = selectedSubproduct;
    
    if (selectedSubproduct == nil) [self.subproductError showAnimated];
    else [self.subproductError hideAnimated];
}
- (EntitySearchResponse *)selectedSubproduct {
    return _selectedSubproduct;
}

#pragma MARK all arrays actions

- (void)setAllProducts:(NSArray *)allProducts {
    _allProducts = allProducts;
    
    if (_allProducts.count > 0) {
        [_allProductsButton showAnimated];
    } else {
        [_allProductsButton hideAnimated];
    }
}
- (NSArray *)allProducts {
    return _allProducts;
}

- (void)setAllSubjects:(NSArray *)allSubjects {
    _allSubjects = allSubjects;
    
    if (allSubjects.count > 0) {
        [_allSubjectsButton showAnimated];
    } else {
        [_allSubjectsButton hideAnimated];
    }
}
- (NSArray *)allSubjects {
    return _allSubjects;
}

- (void)setAllSubproducts:(NSArray *)allSubproducts {
    _allSubproducts = allSubproducts;
    
    if (_allSubproducts.count > 0) {
        self.subproductField.enabled = YES;
        [_allSubproductsButton showAnimated];
    } else {
        self.subproductField.enabled = NO;
        [_allSubproductsButton hideAnimated];
    }
}
- (NSArray *)allSubproducts {
    return _allSubproducts;
}

@end
