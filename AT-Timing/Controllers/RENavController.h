//
//  RENavController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <REFrostedViewController/REFrostedViewController.h>

@interface RENavController : REFrostedViewController

@end
