//
//  SettingsController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 04/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"
#import "EntitySearchResponse.h"
#import "AutoComlpeteView.h"
#import "ProductRequest.h"

@interface SettingsController : CoolViewController <AutoCompleteViewDelegate>

@property UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIButton *notificationsTimeButton;
@property (weak, nonatomic) IBOutlet UISwitch *notificationsSwitch;
@property (weak, nonatomic) IBOutlet UILabel *version;
@property (weak, nonatomic) IBOutlet UIButton *productsButton;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ai;

@property ProductRequest *productsRequest;
@property AutoComlpeteView *acView;
@property NSArray *allProducts;
@property (weak, nonatomic) IBOutlet UIView *productView;
@property EntitySearchResponse *selectedProduct;

@end
