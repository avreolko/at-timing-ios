//
//  TimesheetController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 20/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"
#import <AKPickerView/AKPickerView.h>
#import <QuartzCore/QuartzCore.h>
#import "Worklog.h"
#import "CoolWorklogCell.h"
#import "ModalCalendar.h"

#define DEFAULT_DAYS_COUNT 45
#define DATE_FORMAT @"dd.MM.yyyy"
#define NOTIFICATION_TIME @"18:30"

@interface TimesheetController : CoolViewController <AKPickerViewDelegate, AKPickerViewDataSource, UITableViewDelegate, UITableViewDataSource, FSCalendarDelegate, FSCalendarDataSource>

@property (weak, nonatomic) IBOutlet UIButton *logButton;
@property (weak, nonatomic) IBOutlet AKPickerView *picker;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property NSMutableArray *dates;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property NSMutableArray *logs;
@property NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UILabel *emptyTableMessage;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *tableAI;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property NSMutableArray *dayLogs;
@property NSMutableArray *projectLogs;

@property NSMutableDictionary *daySums;
@property UIRefreshControl *refreshControl;

@property ModalCalendar *calendar;
@property (weak, nonatomic) IBOutlet AutoresizingButton *calendarButton;

@property NSDate *logBeginDate;

@end
