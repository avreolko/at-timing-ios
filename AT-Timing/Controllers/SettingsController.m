//
//  SettingsController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 04/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "SettingsController.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    self.version.text = [NSString stringWithFormat:@"Версия: %@ (%@)", version, build];
    
    NSString *showNotif = [[Cache get].objects objectForKey:NOTIF_SHOW_KEY];
    if ([showNotif isEqualToString:@"true"]) {
        [self.notificationsSwitch setOn:YES];
    } else {
        [self.notificationsSwitch setOn:NO];
    }
    
    NSString *savedTime = [[Cache get].objects objectForKey:NOTIF_TIME_KEY];
    if (savedTime) {
        [_notificationsTimeButton setTitle:savedTime forState:UIControlStateNormal];
    } else {
        [[Cache get] setObject:@"18:30" forKey:NOTIF_TIME_KEY];
        [_notificationsTimeButton setTitle:@"18:30" forState:UIControlStateNormal];
    }
    
    [self loadProducts];
    
    self.selectedProduct = [[Cache get].objects objectForKey:SELECTED_PRODUCT_KEY];
    if (self.selectedProduct && _selectedProduct.name.length > 0) {
        _productLabel.text = _selectedProduct.name;
    }
}

- (void)loadProducts {
    [_productsButton hide];
    [_ai startAnimating];
    
//    NSURL *url = [ServerSettings entitySearchURL:ProductID search:@""];
//    [[ServerRequestManager new] GETRequestWithURL:url completion:^(NiceResponse *response) {
//        [_productsButton showAnimated];
//        [_ai stopAnimating];
//
//        NSError *err = nil;
//        _allProducts = [EntitySearchResponse arrayOfModelsFromString:response.dataString error:&err];
//    }];
//
    _productsRequest = [ProductRequest new];
    [_productsRequest load:^(NSArray *products) {
        if (products && products.count > 0) {
            [_productsButton showAnimated];
            [_ai stopAnimating];
            
            _allProducts = products;
        }
        _productsRequest = nil;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)timeButtonTapped:(id)sender {
    if (!self.datePicker) {
        self.datePicker = [[UIDatePicker alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"RU"];
        [self.datePicker setLocale:locale];
        [self.datePicker addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
        self.datePicker.timeZone = [NSTimeZone defaultTimeZone];
        self.datePicker.minuteInterval = 5;
        self.datePicker.datePickerMode = UIDatePickerModeTime;
        [self.datePicker hide];
        [self.view addSubview:self.datePicker];
        [self.datePicker showAnimated];
        
        NSDate *date = [NSDate date];
        NSString *savedTime = [[Cache get].objects objectForKey:NOTIF_TIME_KEY];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
        if (savedTime) {
            NSArray *time = [savedTime componentsSeparatedByString:@":"];
            NSString *hourString = [time objectAtIndex:0];
            NSString *minutesString = [time objectAtIndex:1];
            
            [components setHour: hourString.integerValue];
            [components setMinute: minutesString.integerValue];
        } else {
            [components setHour:18];
            [components setMinute:30];
        }
        NSDate *newDate = [gregorian dateFromComponents: components];
        [self.datePicker setDate:newDate];
        
        self.datePicker.frame = CGRectMake(0, [Utils screenSize].height - self.datePicker.frame.size.height, [Utils screenSize].width, self.datePicker.frame.size.height);
    } else {
        [self.datePicker hideAnimatedWithCompletion:^{
            self.datePicker = nil;
            [self.datePicker removeFromSuperview];
        }];
    }
}

- (void)datePickerValueChanged {
    //    self.datePicker.date
    NSDate *newDate = self.datePicker.date;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:newDate];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    NSString *stringTime = [NSString stringWithFormat:@"%ld:%ld", (long)hour, (long)minute];
    
    [[Cache get] setObject:stringTime forKey:NOTIF_TIME_KEY];
    [_notificationsTimeButton setTitle:stringTime forState:UIControlStateNormal];
}

- (IBAction)showProducts:(id)sender {
    if (_acView) {
        [self.acView removeFromSuperview];
        self.acView = nil;
        
        return;
    }
    
    
    NSMutableArray *acArray = [NSMutableArray new];
    for (EntitySearchResponse *product in _allProducts) {
        [acArray addObject:product.name];
    }
    
    if (acArray.count > 0) {
        CGFloat x = _productView.frame.origin.x + 4;
        CGFloat y = _productView.frame.origin.y + _productView.frame.size.height + 4;
        
        float height = [Utils screenSize].height - (_productView.frame.size.height + _productView.frame.origin.y) - 8;
        
        self.acView = [AutoComlpeteView initWith:[acArray copy] andCoords:CGPointMake(x, y) andDelegate:self w:_productView.frame.size.width - 8 h:height];
        
//        self.acView = [AutoComlpeteView initWith:[acArray copy] andTextField:_productView andDelegate:self];
        [self.view addSubview:self.acView];
    }
}

- (void)didSelectVariant:(NSUInteger)index {
    [self.acView removeFromSuperview];
    self.acView = nil;
    
    self.selectedProduct = [_allProducts objectAtIndex:index];
    self.productLabel.text = self.selectedProduct.name;
    
    [[Cache get] setObject:self.selectedProduct forKey:SELECTED_PRODUCT_KEY];
}

- (IBAction)notificationsSwitch:(id)sender {
    if (self.notificationsSwitch.isOn) {
        [[Cache get] setObject:@"true" forKey:NOTIF_SHOW_KEY];
    } else {
        [[Cache get] setObject:@"false" forKey:NOTIF_SHOW_KEY];
    }
}

@end
