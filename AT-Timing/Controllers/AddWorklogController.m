//
//  AddWorklogController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 23/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "AddWorklogController.h"
#import "AddWorklogRequest.h"
#import "AuthManager.h"
#import "EntityHelper.h"
#import "ProjectsStore.h"
#import "TimesheetController.h"
#import "JiraLogRequest.h"
#import "TicketStore.h"
#import "JiraLogResponse.h"
#import "RecentTicketsDelegate.h"
#import "ProjectsRequest.h"

@interface AddWorklogController ()

@end

@implementation AddWorklogController

@synthesize time = _time;
@synthesize allProjectsShown = _allProjectsShown;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
}

- (void)setup {
    [_allTicketsButton hide];
    [_ticketField hide];
    self.headerLabel.text = [NSString stringWithFormat:@"Новое списание за %@", [Utils getStringFromDate:self.selectedDate withFormat:DATE_FORMAT]];
    [self.ticketField hide];
    _worklogViewHeight.constant = 0;
    [_worklogView hide];
    _textview.placeholder = @"Комментарий...";
    // Do any additional setup after loading the view from its nib.
    self.recentProjectsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.recentTicketsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_projectField addTarget:self
                      action:@selector(proceedProjectSearch)
            forControlEvents:UIControlEventEditingChanged];
    
    [_ticketField addTarget:self
                      action:@selector(proceedTicketSearch)
            forControlEvents:UIControlEventEditingChanged];
    
    if (IS_IPHONE_5) _commentHeight.constant = 50;
}

- (void)viewWillAppear:(BOOL)animated {
    NSString *selectedIndexString = [[Cache get].objects objectForKey:LOG_TYPE_KEY];
    _segmentedControl.selectedSegmentIndex = [selectedIndexString intValue];
    [self tabChanged:_segmentedControl];
    
    [RecentTicketsDelegate get].tableView = self.recentTicketsTable;
    
//    [self loadAllProjects];
    [self loadAllTickets];
    [self.allProjectsButton hideAnimated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)delayedProjectSearch {
    [_projectAI startAnimating];
    
    if (_projectField.text.length == 0) return;
    
    [[ProjectsRequest new] loadWithSearch:_projectField.text completion:^(NSArray *projects) {
        [_projectAI stopAnimating];
        self.projectSearchResults = projects;
        
        NSMutableArray *autocompleteResults = [NSMutableArray new];
        for (ProjectSearchResponse *response in _projectSearchResults) {
            if (response.code) [autocompleteResults addObject:response.code];
        }
        
        if (autocompleteResults.count > 0) {
            self.acView = [AutoComlpeteView initWith:[autocompleteResults copy] andTextField:_projectField andDelegate:self];
            [_projectsView addSubview:self.acView];
        }
    }];
}

- (void)expandWorklogView {
    self.worklogViewHeight.constant = 240;
//    self.recentProjectsYPadding.constant = 40;
    [UIView animateWithDuration:0.4 animations:^{
        _worklogView.alpha = 1;
        [self.view layoutIfNeeded];
    }];
}

- (void)shrinkWorklogView {
    self.worklogViewHeight.constant = 0;
//    self.recentProjectsYPadding.constant = 16;
    [UIView animateWithDuration:0.4 animations:^{
        _worklogView.alpha = 0;
        [self.view layoutIfNeeded];
    }];
}

- (void)didSelectVariant:(NSUInteger)index {
    [self expandWorklogView];
    
    if (_segmentedControl.selectedSegmentIndex == 0) {
        if (self.allProjectsShown) {
            self.allProjectsShown = NO;
            _selectedProject = [_allProjects objectAtIndex:index];
        } else {
            _selectedProject = [_projectSearchResults objectAtIndex:index];
        }
        _projectField.text = _selectedProject.code;
        [_textview becomeFirstResponder];
    } else if (_segmentedControl.selectedSegmentIndex == 1) {
        if (self.allTicketsShown) {
            self.allTicketsShown = NO;
            _selectedTicket = [_allTickets objectAtIndex:index];
        } else {
            _selectedTicket = [_ticketSearchResults objectAtIndex:index];
        }
        [[TicketStore get] addTicket:self.selectedTicket];
        _ticketField.text = _selectedTicket.description;
        [_textview becomeFirstResponder];
    }
}

- (IBAction)addTime:(id)sender {
    UIButton *button = (UIButton *)sender;
    self.time += button.tag;
}

- (void)setTime:(long)time {
    _time = time;
    
    [self calcTimeField];
}

- (long)time {
    return _time;
}

- (void)calcTimeField {
    _timeField.text = [Utils hoursAndMinutesFromMinutes:self.time];
}

- (IBAction)clearTime:(id)sender {
    self.time = 0;
}

- (void)passData:(id)object {
    self.selectedDate = (NSDate *)object;
}

- (IBAction)addWorklog:(id)sender {
	if (self.selectedProject.needComment && self.textview.text.length < 4) {
		[self.textview setRed:YES];
		return;
	} else {
		[self.textview setRed:NO];
	}

	if (self.time == 0) {
        [self presentViewController:[[AlertManager get] alertForStatus:FILL_TIME] animated:YES completion:nil];
        return;
    }
    
    [self.textview resignFirstResponder];
    [self.projectField resignFirstResponder];
    
    if (self.selectedProject == nil) return;
    
    AddWorklogRequest *request = [AddWorklogRequest new];
    request.changeDate = [Utils getChangedDateWithDate:[NSDate date]];
    request.startDate = [Utils getChangedDateWithDate:self.selectedDate];
    request.objectId = [[Utils generateUUID] lowercaseString];
    request.employee = [AuthManager get].profile.employeeID;
    request.isNewItem = YES;
    request.project = self.selectedProject.objectId;
    request.entityId = TimesheetID;
    request.comment = self.textview.text;
    request.value = [NSString stringWithFormat:@"%ld", self.time * 60];
    request.ticket = @"";
    request.statusTime = StatusTimeID;
    
    NSURL *url = [ServerSettings editEntityURL:request.objectId];
    
    [LoadingBlocker showOn:self.view light:YES];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:request.toJSONString completion:^(NiceResponse *response) {
        [LoadingBlocker hideFrom:self.view];
        if (response.statusCode == 200) {
            [[ProjectsStore get] addProjectWithResponse:self.selectedProject];
            [self addLocalLog:request.objectId];
            [self.navigationController popViewControllerAnimated:YES];
        } else if ([response.dataString containsString:@"Document contains a link named project to the following document"]) {
            [[AlertManager get] show:PROJECT_DOES_NOT_EXIST in:self];
        }
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)addLocalLog:(NSString *)logID {
    Worklog *log = [Worklog new];
    log.id = logID;
    log.login = [AuthManager get].profile.login;
    log.minutes = [NSNumber numberWithLong:self.time];
    log.comment = self.textview.text;
    log.date = self.selectedDate;
    log.projectCode = self.selectedProject.code;
    log.projectId = self.selectedProject.objectId;
    log.ticket = self.selectedTicket.ticketCode;
    log.ticketName = self.selectedTicket.ticketTitle;
    log.ticket = @"";
    log.ticketName = @"";
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:log forKey:@"worklog"];
    [[NSNotificationCenter defaultCenter] postNotificationName:ADD_LOCAL_LOG_EVENT object:self userInfo:dictionary];
}

- (IBAction)addProject:(id)sender {
    [self push:AddProject];
}

#pragma mark recent projects table view

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger count = [ProjectsStore get].recentProjects.count;
    if (count == 0) [self.recentProjectsView hide];
    else if (count > 0 && _segmentedControl.selectedSegmentIndex == 0) [self.recentProjectsView show];
    
    return count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recentProjectCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"recentProjectCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    BriefProjectInfo *project = [[ProjectsStore get].recentProjects objectAtIndex:indexPath.row];
    cell.textLabel.text = project.code;
    cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == _recentProjectsTableView) {
        BriefProjectInfo *project = [[ProjectsStore get].recentProjects objectAtIndex:indexPath.row];
        if (project == nil) return;
        
        self.selectedProject = [ProjectSearchResponse new];
        self.selectedProject.code = project.code;
        self.selectedProject.objectId  = project.objectId;
        self.selectedProject.entityId = ProjectID;
        
        self.projectField.text = project.code;
		[self loadProjectWith:project.code];
        [self expandWorklogView];
        [_textview becomeFirstResponder];
    } else if (tableView == _recentTicketsTable) {
        _selectedTicket = [[TicketStore get].recentTickets objectAtIndex:indexPath.row];
        if (_selectedTicket == nil) return;
        
        self.ticketField.text = _selectedTicket.description;
        
        [self expandWorklogView];
        [_textview becomeFirstResponder];
    }
}

- (void)loadProjectWith:(NSString *)code {
	[[ProjectsRequest new] loadWithSearch:_projectField.text completion:^(NSArray *projects) {
		if (projects.count == 1) {
			ProjectSearchResponse *project = [projects objectAtIndex:0];
			self.selectedProject.needComment = project.needComment;
		}
	}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)loadAllProjects {
    if (_segmentedControl.selectedSegmentIndex == 0) [_projectAI startAnimating];
    [self.allProjectsButton hide];
    self.projectField.enabled = NO;
    self.projectField.placeholder = @"Загружаем проекты...";
    NSURL *url = [ServerSettings entitySearchURL:ProjectID search:@""];
    [[ServerRequestManager new] GETRequestWithURL:url completion:^(NiceResponse *response) {
        [_projectAI stopAnimating];
        
        if (response.statusCode == 200) {
            self.projectField.placeholder = @"Введите название проекта";
            self.projectField.enabled = YES;
            NSError *err = nil;
            _allProjects = [ProjectSearchResponse arrayOfModelsFromString:response.dataString error:&err];
            
            for (ProjectSearchResponse *project in _allProjects) {
                NSLog(@"%@", project.code);
            }
            
            if (_allProjects.count > 0 && _segmentedControl.selectedSegmentIndex == 0) [self.allProjectsButton showAnimated];
        } else {
            self.projectField.placeholder = @"Ошибка загрузки проектов!";
        }
    }];
}

- (void)showACWithProjects:(NSArray *)projects {
    NSMutableArray *autocompleteResults = [NSMutableArray new];
    for (ProjectSearchResponse *response in projects) {
        if (response.code) [autocompleteResults addObject:response.code];
    }
    
    if (autocompleteResults.count > 0) {
        self.acView = [AutoComlpeteView initWith:[autocompleteResults copy] andTextField:_projectField andDelegate:self];
        [_projectsView addSubview:self.acView];
    }
}

- (void)proceedProjectSearch {
    self.selectedProject = nil;
    self.time = 0;
    [self shrinkWorklogView];
    
    [self.acView removeFromSuperview];
    self.acView = nil;
    
    if (_projectField.text.length == 0) return;
    
    if (_projectSearchTimer) [_projectSearchTimer invalidate];
    _projectSearchTimer = [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(delayedProjectSearch) userInfo:nil repeats:NO];
    
//    [self localProjectSearch];
}

- (void)localProjectSearch {
    
    self.projectSearchResults = [_allProjects filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(ProjectSearchResponse *project, NSDictionary *bindings) {
        NSString *code = [project.code lowercaseString];
        NSString *searchString = [self.projectField.text lowercaseString];
        if ([code containsString:searchString]) {
            return YES;
        } else {
            return NO;
        }
    }]];
    
    self.allProjectsShown = NO;
    [self showACWithProjects:self.projectSearchResults];
}

- (IBAction)showAllProjects:(id)sender {
    if (self.allProjectsShown) {
        [self.acView removeFromSuperview];
        self.acView = nil;
        self.allProjectsShown = NO;
    } else {
        self.allProjectsShown = YES;
        [self showACWithProjects:_allProjects];
    }
}

- (void)setAllProjectsShown:(BOOL)allProjectsShown {
    _allProjectsShown = allProjectsShown;
    
    if (_allProjectsShown) {
        [AnimationHelper rotateUP:_allProjectsButton duration:0.3];
    } else {
        [AnimationHelper rotateDown:_allProjectsButton duration:0.3];
    }
}

- (BOOL)allProjectsShown {
    return _allProjectsShown;
}

- (IBAction)tabChanged:(id)sender {
    self.time = 0;
    self.selectedProject = nil;
    self.selectedTicket = nil;
    self.projectField.text = @"";
    self.ticketField.text = @"";
    [self.acView removeFromSuperview];
    self.acView = nil;
    
    [self shrinkWorklogView];
    UISegmentedControl *control = (UISegmentedControl *)sender;
    if (control.selectedSegmentIndex == 0) {
        [[Cache get] setObject:@"0" forKey:LOG_TYPE_KEY];
        [self.projectField showAnimated];
        [self.ticketField hideAnimated];
        [self.addProjectButton showAnimated];
        [self.allTicketsButton hideAnimated];
        if(_allProjects.count > 0 && !_projectAI.isAnimating) [self.allProjectsButton showAnimated];
        [self.recentProjectsView showAnimated];
        [self.recentTicketsView hideAnimated];
        [_projectAI showAnimated];
    } else {
        [[Cache get] setObject:@"1" forKey:LOG_TYPE_KEY];
        [self.projectField hideAnimated];
        [self.ticketField showAnimated];
        [_projectAI hideAnimated];
        [self.addProjectButton hideAnimated];
        if(_allTickets.count > 0) [self.allTicketsButton showAnimated];
        [self.allProjectsButton hideAnimated];
        [self.recentProjectsView hideAnimated];
        [self.recentTicketsView showAnimated];
    }
}

- (void)loadAllTickets {
    _allTickets = [[Cache get].objects objectForKey:TICKETS_KEY];
    if (_allTickets.count > 0 && _segmentedControl.selectedSegmentIndex == 1) {
        [self.allTicketsButton show];
    } else {
        [self.allTicketsButton hide];
    }
    
    NSURL *url = [ServerSettings allTicketsURL];
//    self.ticketField.placeholder = @"Загружаем список последних задач...";
    [[ServerRequestManager new] POSTRequestWithURL:url andData:nil completion:^(NiceResponse *response) {
        NSError *error;
        NSArray *loadedTickets = [JiraTicket arrayOfModelsFromString:response.dataString error:&error];
        if (loadedTickets.count > 0) {
            _allTickets = loadedTickets;
            [[Cache get] setObject:_allTickets forKey:TICKETS_KEY];
            self.ticketField.enabled = YES;
            if (_segmentedControl.selectedSegmentIndex == 1) [self.allTicketsButton showAnimated];
            self.ticketField.placeholder = @"Введите название задачи";
            
            if (_allTicketsShown) {
                [self.acView removeFromSuperview];
                self.acView = nil;
                
                [self showACWithTickets:_allTickets];
            }
        } else {
            self.ticketField.placeholder = @"У вас нет задач в Jira";
        }
    }];
}

- (IBAction)showAllTickets:(id)sender {
    [self.acView removeFromSuperview];
    self.acView = nil;
    if (self.allTicketsShown) {
        
        self.allTicketsShown = NO;
    } else {
        if (_allTickets.count > 0) {
            _allTicketsShown = YES;
            [self showACWithTickets:_allTickets];
        }
    }
}

- (void)showACWithTickets:(NSArray *)tickets {
    NSMutableArray *autocompleteResults = [NSMutableArray new];
    for (JiraTicket *ticket in tickets) {
        NSString *ticketDescription = ticket.description;
        [autocompleteResults addObject:ticketDescription];
    }
    
    if (autocompleteResults.count > 0) {
        self.acView = [AutoComlpeteView initWith:autocompleteResults andTextField:_ticketField andDelegate:self];
        [_projectsView addSubview:self.acView];
    }
}

- (void)proceedTicketSearch {
    [self.acView removeFromSuperview];
    self.acView = nil;
    self.selectedTicket = nil;
    self.allTicketsShown = NO;
    [self shrinkWorklogView];
    
    if ([_ticketField.text isEqualToString:@""]) return;
    
    _ticketSearchResults = [_allTickets filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(JiraTicket *ticket, NSDictionary *bindings) {
        NSString *title = [ticket.ticketTitle lowercaseString];
        NSString *code = [ticket.ticketCode lowercaseString];
        NSString *searchString = [self.ticketField.text lowercaseString];
        if ([title containsString:searchString] || [code containsString:searchString]) {
            return YES;
        } else {
            return NO;
        }
    }]];
    
    [_ticketSearchTimer invalidate];
    if (_ticketSearchResults.count == 0) {
        _ticketSearchTimer = [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(delayedTicketSearch) userInfo:nil repeats:NO];
    }

    [self showACWithTickets:_ticketSearchResults];
}

- (void)delayedTicketSearch {
    NSURL *url = [ServerSettings ticketSearchURL:self.ticketField.text];
    [_projectAI startAnimating];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:nil completion:^(NiceResponse *response) {
        [self.acView removeFromSuperview];
        self.acView = nil;
        
        [_projectAI stopAnimating];
        NSError *error;
        NSArray *loadedTickets = [JiraTicket arrayOfModelsFromString:response.dataString error:&error];
        if (loadedTickets.count > 0) {
            _ticketSearchResults = loadedTickets;
            [self showACWithTickets:_ticketSearchResults];
        }
    }];
}

- (IBAction)addWorklogByTicket:(id)sender {
    if (self.time == 0) {
        [self presentViewController:[[AlertManager get] alertForStatus:FILL_TIME] animated:YES completion:nil];
        return;
    }
    
    if (self.selectedTicket == nil) return;
    
    [self.textview resignFirstResponder];
    [self.ticketField resignFirstResponder];
    
    
    JiraLogRequest *request = [JiraLogRequest new];
    request.started = [Utils isoStringFromDate:self.selectedDate];
    request.timeSpentSeconds = (int)self.time*60;
    request.comment = self.textview.text;

    NSURL *url = [ServerSettings jiraLogURL:self.selectedTicket.ticketCode];
    NSString *datastring = request.toJSONString;
    
    [LoadingBlocker showOn:self.view light:YES];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:datastring completion:^(NiceResponse *response) {
        [LoadingBlocker hideFrom:self.view];
        if (response.statusCode == 201) {
            NSLog(@"JIRA WORKLOG SUCCESS!");
            NSError *err = [NSError new];
            JiraLogResponse *jiraResponse = [[JiraLogResponse alloc] initWithString:response.dataString error:&err];
            
            [self addLocalLog:jiraResponse.objectId];
            [[TicketStore get] addTicket:self.selectedTicket];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            NSLog(@"JIRA LOG STATUS: %ld", (long)response.statusCode);
        }
    }];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteAction = nil;
    
    if (tableView == self.recentTicketsTable) {
        deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Удалить"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            [[TicketStore get].recentTickets removeObjectAtIndex:indexPath.row];
            [[TicketStore get] save];
            [self.recentTicketsTable reloadData];
        }];
    } else if (tableView == self.recentProjectsTableView) {
        deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Удалить"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            [[ProjectsStore get].recentProjects removeObjectAtIndex:indexPath.row];
            [[ProjectsStore get] save];
            [self.recentProjectsTableView reloadData];
        }];
    }
    
    deleteAction.backgroundColor = [Utils getColorFromInt:0xDF3636];
    
    return @[deleteAction];
}

@end
