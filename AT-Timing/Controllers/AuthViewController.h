//
//  ViewController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoolViewController.h"
#import "Profile.h"

@interface AuthViewController : CoolViewController

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panelY;

@property NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIImageView *errorImage;

@end

