//
//  AddWorklogController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 23/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"
#import "ProjectSearchResponse.h"
#import "AutoComlpeteView.h"
#import "CoolTextView.h"
#import "Worklog.h"
#import "JiraTicket.h"

@interface AddWorklogController : CoolViewController <UITextFieldDelegate, AutoCompleteViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *projectsView;
@property (weak, nonatomic) IBOutlet UITextField *projectField;
@property (weak, nonatomic) IBOutlet UITextField *ticketField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *projectAI;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIView *worklogView;
@property (weak, nonatomic) IBOutlet UIView *recentProjectsView;
@property (weak, nonatomic) IBOutlet UIView *recentTicketsView;
@property (weak, nonatomic) IBOutlet UITableView *recentProjectsTableView;
@property (weak, nonatomic) IBOutlet UITableView *recentTicketsTable;
@property (weak, nonatomic) IBOutlet UIButton *allProjectsButton;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *allTicketsButton;


@property (weak, nonatomic) IBOutlet UIButton *addProjectButton;
@property (weak, nonatomic) IBOutlet CoolTextView *timeField;
@property (weak, nonatomic) IBOutlet CoolTextView *textview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *worklogViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentProjectsYPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentHeight;

@property NSTimer *projectSearchTimer;
@property NSTimer *ticketSearchTimer;

@property NSArray *projectSearchResults;
@property NSArray *allProjects;
@property NSArray *ticketSearchResults;
@property NSArray *allTickets;
@property ProjectSearchResponse *selectedProject;
@property JiraTicket *selectedTicket;

@property NSDate *selectedDate;
@property AutoComlpeteView *acView;
@property long time;

@property BOOL allProjectsShown;
@property BOOL allTicketsShown;

@end
