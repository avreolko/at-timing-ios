//
//  AddProjectController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 30/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"
#import "EntitySearchResponse.h"
#import "AutocompleteField.h"
#import "SubjectRequest.h"
#import "ProductRequest.h"
#import "SubproductRequest.h"

@interface AddProjectController : CoolViewController <AutoCompleteViewDelegate, AutocompleteFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UITextField *subjectField;
@property (weak, nonatomic) IBOutlet UITextField *productField;
@property (weak, nonatomic) IBOutlet UITextField *subproductField;

@property AutoComlpeteView *acView;

@property SubjectRequest *subjectsRequest;
@property ProductRequest *productsRequest;
@property SubproductRequest *subproductsRequest;

@property NSArray *subjectSearchResults;
@property NSArray *productSearchResults;
@property NSArray *subproductSearchResults;

@property NSArray *allProducts;
@property NSArray *allSubjects;
@property NSArray *allSubproducts;

@property BOOL allSubjectsShown;
@property BOOL allProductsShown;
@property BOOL allSubproductsShown;

@property EntitySearchResponse *selectedSubject;
@property EntitySearchResponse *selectedProduct;
@property EntitySearchResponse *selectedSubproduct;

@property NSArray *allProjects;

@property (weak, nonatomic) IBOutlet UIButton *allSubjectsButton;
@property (weak, nonatomic) IBOutlet UIButton *allProductsButton;
@property (weak, nonatomic) IBOutlet UIButton *allSubproductsButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *subjectsAI;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *productsAI;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *subproductsAI;

@property (weak, nonatomic) IBOutlet UIImageView *subjectError;
@property (weak, nonatomic) IBOutlet UIImageView *productError;
@property (weak, nonatomic) IBOutlet UIImageView *subproductError;

@end
