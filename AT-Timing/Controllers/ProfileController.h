//
//  ProfileController.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"

@interface ProfileController : CoolViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableDictionary *profileDict;

@end
