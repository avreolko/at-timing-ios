//
//  TimesheetController.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 20/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "TimesheetController.h"
#import "EntityHelper.h"
#import "AuthManager.h"
#import "WorklogCell.h"
#import "DeleteEntityRequest.h"
#import "BriefProjectInfo.h"
#import "NotificationHelper.h"
#import "AuthViewController.h"
#import <REFrostedViewController/REFrostedViewController.h>
#import "HolidayChecker.h"
#import "WorklogCalendarDay.h"

@interface TimesheetController ()

@end

@implementation TimesheetController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.logButton.hidden = YES;
    self.hideNavigationBar = YES;
    
    [self setup];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addWorklogFromCell:) name:ADD_WORKLOG_FROM_CELL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(worklogChanged) name:UPDATE_LOCAL_WORKLOG_EVENT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localLogAdded:) name:ADD_LOCAL_LOG_EVENT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editWorklogFromCell:) name:EDIT_LOG_EVENT_CELL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteWorklogFromCell:) name:DELETE_LOG_EVENT_CELL object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addWorklogFromCellWithTicket:) name:ADD_TICKET_WORKLOG_FROM_CELL object:nil];
    
    // Do any additional setup after loading the view from its nib.
    [self loadTimesheet];
    [self calculateSumWorktimes]; // for notifications
}

- (void)addWorklogFromCell:(NSNotification *)note {
    Worklog *log = [note.userInfo objectForKey:@"worklog"];
    BriefProjectInfo *project = [BriefProjectInfo new];
    project.code = log.projectCode;
    project.objectId = log.projectId;
    project.changeDate = [self.dates objectAtIndex:self.picker.selectedItem];
    if (log.ticket.length > 0) project.ticket = log.ticket; project.ticketName = log.ticketName;
    [self present:EditWorklog withData:project];
}

- (void)editWorklogFromCell:(NSNotification *)note {
    Worklog *log = [note.userInfo objectForKey:@"worklog"];
    [self present:EditWorklog withData:log];
}

- (void)deleteWorklogFromCell:(NSNotification *)note {
    Worklog *log = [note.userInfo objectForKey:@"worklog"];
    [self.logs removeObject:log];
    [self calculateSumWorktimes];
    [self getDayLogs];
    
    NSLog(@"%@", log.ticket);

    if (log.ticket.length > 0 && NO == [log.ticket containsString:@"OTRS"]) {
        NSURL *url = [ServerSettings editEntityURL:log.id];
        [[ServerRequestManager new] GETRequestWithURL:url completion:^(NiceResponse *response) {
            NSError *err = nil;
            //    NSArray *array = [EntityResponse arrayOfModelsFromString:json error:&err];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response.dataString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
            NSString *logid = [dict objectForKey:@"logid"];
            
            NSURL *deleteURL = [ServerSettings jiraDeleteLogURL:log.ticket logID:logid];
            [[ServerRequestManager new] DELETERequestWithURL:deleteURL completion:^(NiceResponse *response) {
                NSLog(@"JIRA DELETE RESPONSE CODE: %ld", (long)response.statusCode);
            }];
        }];
    } else {
        [self deleteWorklog:log.id];
    }
}

- (void)worklogChanged {
    [self.tableView reloadData];
    [self calculateSumWorktimes];
}

- (void)localLogAdded:(NSNotification *)note {
    Worklog *log = [note.userInfo objectForKey:@"worklog"];
    [self.logs insertObject:log atIndex:0];
    [self calculateSumWorktimes];
    [self getDayLogs];
}

- (void)viewWillAppear:(BOOL)animated {
//    [self loadTimesheet];
}

- (void)setup {
    self.picker.delegate = self;
    self.picker.dataSource = self;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.5;
    self.shadowView.layer.shadowRadius = 4;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.contentInset = UIEdgeInsetsZero;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView:)];
    [self.tableView addGestureRecognizer:tap];
    
    [self.emptyTableMessage hide];
    
    [self formatHeaderLabel:[NSDate date]];
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 90;
    
    if (!IS_IPHONE_4_OR_LESS && SYSTEM_VERSION_GREATER_THAN(@"9.0") ) {
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self
                                action:@selector(loadTimesheet)
                      forControlEvents:UIControlEventValueChanged];
        self.tableView.refreshControl = self.refreshControl;
    }
}

//- (void)createDateArray {
//    self.dates = [NSMutableArray new];
//    int i = -[self getDaysCount];
//    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
//    NSCalendar *theCalendar = [NSCalendar currentCalendar];
//    while (i <= 0) {
//        dayComponent.day = i;
//        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
//        [self.dates addObject:nextDate];
//        i++;
//    }
//}

- (void)createDateArray {
    self.dates = [NSMutableArray new];

	NSNumber *timestamp = [AuthManager get].profile.worklogTimeStamp;
    NSDate *firstDate = [[NSDate alloc] initWithTimeIntervalSince1970:timestamp.longValue/1000];

    [self.dates addObject:firstDate];

    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = firstDate;
    while (true) {
        dayComponent.day = 1;
        nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:nextDate options:0];
        if ([nextDate timeIntervalSince1970] < [[NSDate date] timeIntervalSince1970]) {
            [self.dates addObject:nextDate];
        } else {
            break;
        }
    }
}

- (int)getDaysCount {
    if (self.logs.count == 0) {
        return DEFAULT_DAYS_COUNT;
    } else {
        NSDate *date = NSDate.date;
        for (Worklog *log in self.logs) {
            if ([log.date earlierDate: date]) {
                date = log.date;
            }
        }
        
        long secondsBetween = NSDate.date.timeIntervalSince1970 - date.timeIntervalSince1970;
        
        int daysCount = roundf((float)secondsBetween / 86400);
        return daysCount;
    }
}

- (void)loadTimesheet {
    NSURL *url = [ServerSettings timesheetURL:[AuthManager get].profile.employeeID];
    EntityRequest *request = [EntityHelper getTimesheetRequest:[AuthManager get].profile.login];
    request.limit = 9999;
    [request.attributes addObject:@"employee.login"];
    [request.attributes addObject:@"value"];
    [request.attributes addObject:@"startDate"];
    [request.attributes addObject:@"project.code"];
    [request.attributes addObject:@"comment"];
    [request.attributes addObject:@"project"];
    [request.attributes addObject:@"ticket.projectCode"];
    [request.attributes addObject:@"ticket.summary"];
    [request.attributes addObject:@"statusTime.Code"];
    [request.attributes addObject:@"PO.fio"];
    [request.attributes addObject:@"commentPO"];
    [request.attributes addObject:@"logid"];
    
//    [request setDatePastMonth];
    [request addPreviousMonthCondition];
    request.sort = @"startDate";
    request.sortAsc = false;
    
    [self.picker prepareView];
    NSString *json = request.toJSONString;
    
    if (!_refreshControl.isRefreshing) {
        [self.contentView hide];
        [_tableAI startAnimating];
    }
    
    [[ServerRequestManager new] POSTRequestWithURL:url andData:json completion:^(NiceResponse *response) {
        [self.refreshControl endRefreshing];
        self.logs = [EntityHelper getWorklogsFromJson:response.dataString];
        [self calculateSumWorktimes];
        [self getDayLogs];
        [self createDateArray];
        if (!self.selectedDate) [self.picker selectItem:self.dates.count-1 animated:NO];
        
        [_tableAI stopAnimating];
        [self.contentView showAnimated];
//        NSLog(@"response json: %@", response.dataString);
    }];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item {
    self.selectedDate = [self.dates objectAtIndex:item];
    [self formatHeaderLabel:[self.dates objectAtIndex:item]];
    
    [self getDayLogs];
}

- (void)calculateSumWorktimes {
    NSDate *now = [NSDate date];
    // +1 for between check in calendar
    self.logBeginDate = [now dateByAddingTimeInterval:-([self getDaysCount] + 1)*24*60*60];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:FALSE];
    [self.logs sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMAT];
    
    self.daySums = [NSMutableDictionary new];
    
    Worklog *firstLog = self.logs.firstObject;
    NSString *compareString = [dateFormat stringFromDate:firstLog.date];
    int sum = 0;
    int todaySum = 0;

    for (Worklog *log in self.logs) {
        WorklogCalendarDay *worklogDay = nil;
        WorklogCalendarDay *savedWorklogDay = [self.daySums objectForKey:[dateFormat stringFromDate:log.date]];
        
        if (savedWorklogDay) worklogDay = savedWorklogDay;
        else worklogDay = [WorklogCalendarDay new];
        
        if (!log.date) return;
        
        if (![compareString isEqualToString:[dateFormat stringFromDate:log.date]]) { sum = 0; }
        
        sum += [log.minutes intValue];
        worklogDay.minutes = [NSNumber numberWithInteger:sum];
        
        if (log.isDeclined) [worklogDay.additionalData setObject:[[NSNumber alloc] initWithInt:1] forKey:@"isDeclinedDay"];
        
        [self.daySums setObject:worklogDay forKey:[dateFormat stringFromDate:log.date]];
        
        compareString = [dateFormat stringFromDate:log.date];

        if ([compareString isEqualToString:[dateFormat stringFromDate:[NSDate date]]]) {
            todaySum = worklogDay.minutes.intValue;
        }
    }
    
    if (todaySum < 60*8) {
        [self addNotification];
    } else {
        [self removeNotification];
    }
    
    [self.picker reloadData];
}

- (void)addNotification {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    if ([[NSCalendar currentCalendar] isDateInWeekend:[NSDate date]]) {
        return;
    }
    
    NSString *savedTime = [[Cache get].objects objectForKey:NOTIF_TIME_KEY];
    NSString *showNotif = [[Cache get].objects objectForKey:NOTIF_SHOW_KEY];
    if (!showNotif || [showNotif isEqualToString:@"true"]) {
        if (savedTime) {
            [NotificationHelper addLocalNotificationAtTime:savedTime message:@"Пора списать время!"];
        } else {
            [NotificationHelper addLocalNotificationAtTime:NOTIFICATION_TIME message:@"Пора списать время!"];
        }
    }
}

- (void)removeNotification {
    [NotificationHelper removeAllNotifications];
}

- (void)getDayLogs {
    _dayLogs = [NSMutableArray new];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMAT];
    
    for (Worklog *log in self.logs) {
        if ([[dateFormat stringFromDate:log.date] isEqualToString:[dateFormat stringFromDate:self.selectedDate]]) {
            [_dayLogs addObject:log];
        }
    }
    
    BOOL forbidChanges = NO;
    for (Worklog *log in self.dayLogs) {
        if ([log isClosed]) {
            forbidChanges = YES;
        }
    }
    
    self.logButton.hidden = forbidChanges;
    
    // sort logs by projects
    _projectLogs = [NSMutableArray new];
    NSSet *uniqueStates = [NSSet setWithArray:[_dayLogs valueForKey:@"projectCode"]];
    for (NSString *project in uniqueStates) {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"projectCode == %@", project];
        NSMutableArray *arrayByProject = [[_dayLogs filteredArrayUsingPredicate:pred] mutableCopy];
        
        
        pred = [NSPredicate predicateWithFormat:@"ticket.length == 0"];
        arrayByProject = [[arrayByProject filteredArrayUsingPredicate:pred] mutableCopy];
        if (arrayByProject.count > 0) [_projectLogs addObject:arrayByProject];
    }
    // end
    
    
    // sort logs by ticket
    NSSet *uniqueStatesTicket = [NSSet setWithArray:[_dayLogs valueForKey:@"ticket"]];
    for (NSString *ticket in uniqueStatesTicket) {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"ticket == %@", ticket];
        NSMutableArray *arrayByTicket = [[_dayLogs filteredArrayUsingPredicate:pred] mutableCopy];

        pred = [NSPredicate predicateWithFormat:@"ticket.length > 0"];
        arrayByTicket = [[arrayByTicket filteredArrayUsingPredicate:pred] mutableCopy];
        
        if (arrayByTicket.count > 0) [_projectLogs addObject:arrayByTicket];
    }
    // end
    
    [self.tableView layoutIfNeeded];
    [self.tableView reloadData];
}

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView {
    return self.dates.count;
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item {
    NSDate *date = [self.dates objectAtIndex:item];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"E"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"ru_RU"];
    [dateFormatter setLocale:locale];
    
    NSString *dayString = [dateFormatter stringFromDate:date];
    dayString = [dayString stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                        withString:[[dayString substringToIndex:1] capitalizedString]];
    
    
    WorklogCalendarDay *worklogDay = [self.daySums objectForKey:[Utils stringFromDate:date withFormat:DATE_FORMAT]];
    NSNumber *minutes = worklogDay.minutes;
    if (minutes == nil) minutes = [NSNumber numberWithInt:0];
    NSString *sumString = [NSString stringWithFormat:@"\n%d:%02d", minutes.intValue/60, minutes.intValue%60];
    
    return [dayString stringByAppendingString:sumString];
}

- (void)pickerView:(AKPickerView *)pickerView configureLabel:(UILabel *const)label forItem:(NSInteger)item {
    label.numberOfLines = 0;
    
    label.font = [label.font fontWithSize:14];
    
    NSRange r1 = [label.text rangeOfString:@"\n"];
    NSRange r2 = [label.text rangeOfString:@":"];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    NSString *hoursString = [label.text substringWithRange:rSub];
    int hours = [hoursString intValue];
    
    NSDate *date = [self.dates objectAtIndex:item];
    WorklogCalendarDay *worklogDay = [self.daySums objectForKey:[Utils getStringFromDate:date withFormat:DATE_FORMAT]];
    BOOL isDeclined = [worklogDay.additionalData objectForKey:@"isDeclinedDay"] != nil;
    if ([[HolidayChecker get] isThisHoliday:date]) {
        [label setTextColor:[Utils getColorFromInt:0xdd9ae5]];
    } else if ((hours < 8 && !([label.text containsString:@"Вс"] || [label.text containsString:@"Сб"])) ||
               isDeclined) {
        [label setTextColor:[UIColor redColor] fromOccurenceOfString:@"\n" toOccurenceOfString:@""];
    }
}

- (void)formatHeaderLabel:(NSDate *)date {
    NSString *labelText = [NSString stringWithFormat:@"Списания за %@", [Utils getStringFromDate:date withFormat:DATE_FORMAT]];
    self.headerLabel.text = labelText;
}

- (IBAction)addWorklog:(id)sender {
    [self push:AddWorklog withData:self.selectedDate];
}

#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dayLogs.count == 0 && self.logs.count > 0) {
        [self.emptyTableMessage show];
    } else {
        [self.emptyTableMessage hide];
    }
    
//    return self.dayLogs.count;
    return self.projectLogs.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"coolWorklogCell";
    CoolWorklogCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        [self.tableView registerNib:[UINib nibWithNibName:@"CoolWorklogCell" bundle:nil] forCellReuseIdentifier:cellID];
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    }
    
    BOOL forbidChanges = NO;
    for (Worklog *log in self.dayLogs) {
        if ([log isClosed]) {
            forbidChanges = YES;
        }
    }
    cell.createLogButton.hidden = forbidChanges;
    
    [cell configureWithLogs:[self.projectLogs objectAtIndex:indexPath.row]];
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    return cell;
}

- (void)didTapOnTableView:(UIGestureRecognizer *)recognizer {
    if (_calendar.alpha == 1) {
        [Utils unblurView:self.tableView];
        [_calendar hideAnimated];
    }
}

- (void)deleteWorklog:(NSString *)logID {
    NSURL *url = [ServerSettings deleteEntityURL];
    DeleteEntityRequest *request = [DeleteEntityRequest new];
    request.objectIds = [NSMutableArray new];
    [request.objectIds addObject:logID];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:request.toJSONString completion:^(NiceResponse *response) {
        NSLog(@"Worklog delete success!");
    }];
}

- (IBAction)calendarButtonTapped:(id)sender {
    if (!_calendar) {
        _calendar = [ModalCalendar loadWithAnchorView:_calendarButton andDelegate:self];
        [_calendar hide];
        [_calendar.calendar reloadData];
        [self.view addSubview:_calendar];
    }
    
    if (_calendar.alpha == 1) {
        [Utils unblurView:self.tableView];
        [_calendar hideAnimated];
    } else {
        [Utils blurView:self.tableView];
        [_calendar.calendar selectDate:_selectedDate];
        [_calendar showAnimated];
    }
}

//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date {
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    if (![Utils date:date isBetweenDate:_logBeginDate andDate:[NSDate date]]) {
        return [UIColor clearColor];
    }
    
    WorklogCalendarDay *worklogDay = [self.daySums objectForKey:[Utils stringFromDate:date withFormat:DATE_FORMAT]];
    BOOL isDeclined = [worklogDay.additionalData objectForKey:@"isDeclinedDay"] != nil;
    NSNumber *minutes = worklogDay.minutes;
    
    if (isDeclined) {
        return [Utils getColorFromInt:0xFFD0D0];
    }
    
    NSCalendar *nscalendar = [NSCalendar currentCalendar];
    if ([[HolidayChecker get] isThisHoliday:date]) {
        return [Utils getColorFromInt:0xdd9ae5];
    } else if([[HolidayChecker get] isThisSpecialWorkingDay:date]) {
        int hours = minutes.intValue / 60;
        return hours < 8 ? [Utils getColorFromInt:0xFFD0D0] : [Utils getColorFromInt:0xD0FFD3];
    } else if (minutes && ![nscalendar isDateInWeekend:date] && ![nscalendar isDateInToday:date]) {
        int hours = minutes.intValue / 60;
        return hours < 8 ? [Utils getColorFromInt:0xFFD0D0] : [Utils getColorFromInt:0xD0FFD3];
    } else if ([nscalendar isDateInToday:date]) {
        return [Utils getColorFromInt:MAIN_COLOR];
    } else if (!minutes && ![nscalendar isDateInWeekend:date]) {
        if (([_logBeginDate compare:date] == NSOrderedAscending) && ([[NSDate date] compare:date] == NSOrderedDescending)) {
            return [Utils getColorFromInt:0xFFD0D0];
        } else {
            return [UIColor clearColor];
        }
    } else {
        return [UIColor clearColor];
    }
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition {
    NSString *selectedDateString = [Utils stringFromDate:date withFormat:DATE_FORMAT];
    for (NSDate *date in self.dates) {
        NSString *dateString = [Utils stringFromDate:date withFormat:DATE_FORMAT];
        if ([dateString isEqualToString:selectedDateString]) {
            NSUInteger index = [self.dates indexOfObject:date];
            [self.picker selectItem:index animated:YES];
            break;
        }
    }
}

- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar {
//    NSDate *date = NSDate.date;
//    for (Worklog *log in self.logs) {
//        if ([log.date earlierDate: date]) {
//            date = log.date;
//        }
//    }

    return [Utils getBeginningOfMonthWith:-1];
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar {
    return [NSDate date];
}

@end
